'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">gateway documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AppModule-be9f7af5cfe6f3296c42093019a876c0"' : 'data-target="#xs-controllers-links-module-AppModule-be9f7af5cfe6f3296c42093019a876c0"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AppModule-be9f7af5cfe6f3296c42093019a876c0"' :
                                            'id="xs-controllers-links-module-AppModule-be9f7af5cfe6f3296c42093019a876c0"' }>
                                            <li class="link">
                                                <a href="controllers/AuthController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AuthController</a>
                                            </li>
                                            <li class="link">
                                                <a href="controllers/BusinessController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BusinessController</a>
                                            </li>
                                            <li class="link">
                                                <a href="controllers/NotificationController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NotificationController</a>
                                            </li>
                                            <li class="link">
                                                <a href="controllers/UserController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UserController</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AuthModule.html" data-type="entity-link">AuthModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AuthModule-a8a834c2adeebf7afc9f95a596c7b407"' : 'data-target="#xs-injectables-links-module-AuthModule-a8a834c2adeebf7afc9f95a596c7b407"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AuthModule-a8a834c2adeebf7afc9f95a596c7b407"' :
                                        'id="xs-injectables-links-module-AuthModule-a8a834c2adeebf7afc9f95a596c7b407"' }>
                                        <li class="link">
                                            <a href="injectables/AuthService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AuthService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/FacebookAdminStrategy.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>FacebookAdminStrategy</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/FacebookStrategy.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>FacebookStrategy</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/JwtStrategy.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>JwtStrategy</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/LocalStrategy.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>LocalStrategy</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/SocialAccountStrategy.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>SocialAccountStrategy</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/UsersModule.html" data-type="entity-link">UsersModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-UsersModule-27624761e5b9b6643d1bb17da316850e"' : 'data-target="#xs-injectables-links-module-UsersModule-27624761e5b9b6643d1bb17da316850e"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-UsersModule-27624761e5b9b6643d1bb17da316850e"' :
                                        'id="xs-injectables-links-module-UsersModule-27624761e5b9b6643d1bb17da316850e"' }>
                                        <li class="link">
                                            <a href="injectables/UserSagas.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UserSagas</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UsersService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UsersService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#controllers-links"' :
                                'data-target="#xs-controllers-links"' }>
                                <span class="icon ion-md-swap"></span>
                                <span>Controllers</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="controllers-links"' : 'id="xs-controllers-links"' }>
                                <li class="link">
                                    <a href="controllers/AuthController.html" data-type="entity-link">AuthController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/BusinessController.html" data-type="entity-link">BusinessController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/NotificationController.html" data-type="entity-link">NotificationController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/NotificationGateway.html" data-type="entity-link">NotificationGateway</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/SocialController.html" data-type="entity-link">SocialController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/UserController.html" data-type="entity-link">UserController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/WorldController.html" data-type="entity-link">WorldController</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/AddCustomerIdToUserCommand.html" data-type="entity-link">AddCustomerIdToUserCommand</a>
                            </li>
                            <li class="link">
                                <a href="classes/AddCustomerIdToUserCommandHandler.html" data-type="entity-link">AddCustomerIdToUserCommandHandler</a>
                            </li>
                            <li class="link">
                                <a href="classes/AddSocialAccountToUserCommand.html" data-type="entity-link">AddSocialAccountToUserCommand</a>
                            </li>
                            <li class="link">
                                <a href="classes/AddSocialAccountToUserCommandHandler.html" data-type="entity-link">AddSocialAccountToUserCommandHandler</a>
                            </li>
                            <li class="link">
                                <a href="classes/AddSocialAccountToUserEvent.html" data-type="entity-link">AddSocialAccountToUserEvent</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateUserCommand.html" data-type="entity-link">CreateUserCommand</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateUserCommandHandler.html" data-type="entity-link">CreateUserCommandHandler</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateUserEvent.html" data-type="entity-link">CreateUserEvent</a>
                            </li>
                            <li class="link">
                                <a href="classes/Firebase.html" data-type="entity-link">Firebase</a>
                            </li>
                            <li class="link">
                                <a href="classes/GetUserByIdQuery.html" data-type="entity-link">GetUserByIdQuery</a>
                            </li>
                            <li class="link">
                                <a href="classes/GetUserByIdQueryHandler.html" data-type="entity-link">GetUserByIdQueryHandler</a>
                            </li>
                            <li class="link">
                                <a href="classes/GetUsersByIdsQuery.html" data-type="entity-link">GetUsersByIdsQuery</a>
                            </li>
                            <li class="link">
                                <a href="classes/GetUsersByIdsQueryHandler.html" data-type="entity-link">GetUsersByIdsQueryHandler</a>
                            </li>
                            <li class="link">
                                <a href="classes/MongoDatabaseHandle.html" data-type="entity-link">MongoDatabaseHandle</a>
                            </li>
                            <li class="link">
                                <a href="classes/SocketGateway.html" data-type="entity-link">SocketGateway</a>
                            </li>
                            <li class="link">
                                <a href="classes/TypedEventEmitter.html" data-type="entity-link">TypedEventEmitter</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserEventHandle.html" data-type="entity-link">UserEventHandle</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AuthService.html" data-type="entity-link">AuthService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FacebookAdminAuthGuard.html" data-type="entity-link">FacebookAdminAuthGuard</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FacebookAdminStrategy.html" data-type="entity-link">FacebookAdminStrategy</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FacebookAuthGuard.html" data-type="entity-link">FacebookAuthGuard</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FacebookRoleAuthGuard.html" data-type="entity-link">FacebookRoleAuthGuard</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FacebookRoleStrategy.html" data-type="entity-link">FacebookRoleStrategy</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FacebookStrategy.html" data-type="entity-link">FacebookStrategy</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FormDataInterceptor.html" data-type="entity-link">FormDataInterceptor</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FormDataMiddleware.html" data-type="entity-link">FormDataMiddleware</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FormDataPipe.html" data-type="entity-link">FormDataPipe</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/JoiGenericValidationPipe.html" data-type="entity-link">JoiGenericValidationPipe</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/JoiValidationPipe.html" data-type="entity-link">JoiValidationPipe</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/JwtAuthGuard.html" data-type="entity-link">JwtAuthGuard</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/JwtStrategy.html" data-type="entity-link">JwtStrategy</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LocalAuthGuard.html" data-type="entity-link">LocalAuthGuard</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LocalStrategy.html" data-type="entity-link">LocalStrategy</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SocialAccountStrategy.html" data-type="entity-link">SocialAccountStrategy</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserSagas.html" data-type="entity-link">UserSagas</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UsersService.html" data-type="entity-link">UsersService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/RoleAccessGuard.html" data-type="entity-link">RoleAccessGuard</a>
                            </li>
                            <li class="link">
                                <a href="guards/RolesGuard.html" data-type="entity-link">RolesGuard</a>
                            </li>
                            <li class="link">
                                <a href="guards/SocialAccountGuard.html" data-type="entity-link">SocialAccountGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/IAuthPayload.html" data-type="entity-link">IAuthPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IBusinessRequestQuery.html" data-type="entity-link">IBusinessRequestQuery</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ICreateBusinessBody.html" data-type="entity-link">ICreateBusinessBody</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ICreateBusinessPayload.html" data-type="entity-link">ICreateBusinessPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ICreateFireUserParams.html" data-type="entity-link">ICreateFireUserParams</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ICreateMongoUserParams.html" data-type="entity-link">ICreateMongoUserParams</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ICreateUserParams.html" data-type="entity-link">ICreateUserParams</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ICreateUserParamsBase.html" data-type="entity-link">ICreateUserParamsBase</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IDatabase.html" data-type="entity-link">IDatabase</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IDatabaseModels.html" data-type="entity-link">IDatabaseModels</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IFirestoreEntity.html" data-type="entity-link">IFirestoreEntity</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ILoginRequest.html" data-type="entity-link">ILoginRequest</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IMongoEntity.html" data-type="entity-link">IMongoEntity</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IMongoStringEntity.html" data-type="entity-link">IMongoStringEntity</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IMunicipalityPayload.html" data-type="entity-link">IMunicipalityPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/INameCheck.html" data-type="entity-link">INameCheck</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/INameCheck-1.html" data-type="entity-link">INameCheck</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/INotificationAction.html" data-type="entity-link">INotificationAction</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/INotificationCredential.html" data-type="entity-link">INotificationCredential</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/INotificationCredentialsSchema.html" data-type="entity-link">INotificationCredentialsSchema</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IPOJO.html" data-type="entity-link">IPOJO</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IPOJO-1.html" data-type="entity-link">IPOJO</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ISocialAccount.html" data-type="entity-link">ISocialAccount</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ISocialAccountRequest.html" data-type="entity-link">ISocialAccountRequest</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ISocialRegisterRequest.html" data-type="entity-link">ISocialRegisterRequest</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ISocketAction.html" data-type="entity-link">ISocketAction</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ISocketBody.html" data-type="entity-link">ISocketBody</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ITextIndexMap.html" data-type="entity-link">ITextIndexMap</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IUserEntityBase.html" data-type="entity-link">IUserEntityBase</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IUserEntityBaseArrayMap.html" data-type="entity-link">IUserEntityBaseArrayMap</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IUserFireEntity.html" data-type="entity-link">IUserFireEntity</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IUserMongoEntity.html" data-type="entity-link">IUserMongoEntity</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IValuePair.html" data-type="entity-link">IValuePair</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/TypeRecord.html" data-type="entity-link">TypeRecord</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/enumerations.html" data-type="entity-link">Enums</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/typealiases.html" data-type="entity-link">Type aliases</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});