import { createHmac } from 'crypto'
import { Injectable, UnauthorizedException, BadRequestException, Logger, CanActivate, ExecutionContext } from '@nestjs/common'
import { AuthService } from '../services/auth/auth.service'
import { UsersService } from '../../users/services/users.service'
import { checkSigningKeyForCredentials } from '../utility/utility'

interface IAuthSocketPayload {
    user_id: string
    access_token: string
    social_provider: string
}

@Injectable()
export class SocialAccountWsGuard implements CanActivate {
    private readonly logger = new Logger(SocialAccountWsGuard.name)
    private readonly key: string
    // private readonly fbAPIEndpoint =
    //     // 'https://graph.facebook.com/v7.0/oauth/access_token?'
    'https://graph.facebook.com/oauth/access_token?'
    private readonly fbAPIDebugEndpoint =
        'https://graph.facebook.com/v7.0/debug_token?'
    private readonly fb_app_id: string
    private readonly fb_app_secret: string

    constructor(
        private authService: AuthService,
        private usersService: UsersService,
    ) {
        this.key = process.env.ACCOUNT_KEY
        this.fb_app_id = process.env.FACEBOOK_APP_ID
        this.fb_app_secret = process.env.FACEBOOK_APP_SECRET
    }

    async canActivate(context: ExecutionContext) {
        // : boolean | Promise<boolean> | Observable<boolean> {
        try {
            // const request: Request = context.switchToHttp().getRequest()
            const client = context.switchToWs().getClient()
            // const cookies: string[] = client.handshake.headers.cookie.split('; ')
            const request = context.switchToWs().getData<IAuthSocketPayload>()

            const {
                user_id,
                access_token,
                social_provider,
            } = request

            // let { user_id, access_token, social_provider } = request.body

            // if (user_id === undefined ||
            //     access_token === undefined ||
            //     social_provider === undefined) {
            //     let headers = request.headers
            //     user_id = headers.user_id
            //     access_token = headers.access_token
            //     social_provider = headers.social_provider
            // }

            const isSigningKeyOK = checkSigningKeyForCredentials(this.key)
            if (!isSigningKeyOK) {
                throw new BadRequestException()
            }

            if ((social_provider as string).toLowerCase() === 'facebook') {
                let isOK = await this.authService.checkFacebookLogin(
                    this.fbAPIDebugEndpoint,
                    access_token,
                    this.fb_app_id,
                    this.fb_app_secret,
                    user_id,
                )

                if (!isOK) {
                    return false
                }

                const user =
                    await this.usersService.findOneByDocument(user_id)

                if (!user) {
                    this.logger.debug(`User wasn't found`)
                    // throw new UnauthorizedException()
                    return false
                }

                return true
            }

            throw new UnauthorizedException()

        } catch (error) {
            console.debug(error)
            this.logger.error(error)
            // return null
            // throw new UnauthorizedException()
            return false
        }
    }
}