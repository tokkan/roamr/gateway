import axios from 'axios'

interface IFacebookDebugTokenResponse {
    [key: string]: any
    is_valid: boolean
    app_id: string
    user_id: string
}

export const checkFbToken = async (
    fbAPIEndpoint: string,
    access_token: string,
    fbAppId: string,
    fbAppSecret: string
) => {
    let debug_url = `${fbAPIEndpoint}input_token=${access_token}&access_token=${fbAppId}|${fbAppSecret}`
    let response = await axios.get(debug_url)
    let data: IFacebookDebugTokenResponse = await response.data.data
    return data
}