import { Module } from '@nestjs/common'
import { PassportModule } from '@nestjs/passport'
import { JwtModule } from '@nestjs/jwt'
import { UsersModule } from '../users/users.module'
import { AuthService } from './services/auth/auth.service'
import { ConfigService } from '@nestjs/config'
import { LoginService } from './services/login/login.service'

@Module({
  imports: [
    UsersModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.registerAsync({
      useFactory: async (configService: ConfigService) => ({
        secret: process.env.ACCOUNT_KEY,
        signOptions: { expiresIn: '7d' },
      }),
    }),
  ],
  providers: [
    AuthService,
    LoginService,
  ],
  exports: [
    AuthService,
    LoginService,
  ],
})
export class AuthModule { }