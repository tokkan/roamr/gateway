import { Logger } from "@nestjs/common"
import admin from "firebase-admin"

const logger = new Logger(`FirebaseUtility`)

export const checkFirebaseUser = async (
    provider: 'facebook.com',
    user_id: string,
    facebookUserId: string,
) => {
    let firebase_data = await getFirebaseData(
        // provider,
        user_id,
    )

    if (firebase_data === null) {
        return false
    }

    let providerData = getFirebaseProviderData(
        provider,
        facebookUserId,
        firebase_data,
    )

    if (providerData === null) {
        return false
    } else {
        return true
    }
}

export const getFirebaseData = async (
    // provider: 'facebook.com',
    user_id: string,
) => {
    let firebase_data = await admin.auth().getUser(user_id)

    if (!firebase_data ||
        firebase_data === undefined ||
        firebase_data.providerData.length <= 0) {
        logger.debug(`Missing firebase data`)
        // return false
        return null
    }

    return firebase_data
}

export const getFirebaseProviderData = (
    provider: 'facebook.com',
    facebookUserId: string,
    firebaseUserInfo: admin.auth.UserRecord,
) => {
    let fb =
        firebaseUserInfo.providerData.find(f =>
            f.providerId === provider)

    if (fb === undefined ||
        fb.uid !== facebookUserId) {
        logger.debug(`Missing uid attached to Facebook object or it doesn't match user id`)
        return null
    }

    return fb
}