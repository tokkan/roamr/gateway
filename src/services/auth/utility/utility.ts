import { Logger } from "@nestjs/common"

const logger = new Logger(`AuthUtility`)

export const isObjectEmpty = (obj: any) => {
    const isEmpty =
        Object.getOwnPropertyNames(obj).length === 0
    return isEmpty
}

export const checkSigningKeyForCredentials = (
    key: string,
) => {
    if (key === undefined || key.length <= 0) {
        logger.error('Key for signing credentials is not available')
        // throw new BadRequestException()
        return false
    } else {
        return true
    }
}