import { Injectable, Logger } from '@nestjs/common'
import { UsersService } from '../../../users/services/users.service'
import { JwtService } from '@nestjs/jwt'
import { ObjectId } from 'mongodb'
import { DevicePlatforms } from 'src/models/common/common'
import { IUserFireEntity } from '../../../users/models'
import { SocialProviders } from '../../../users/models/social-account'

@Injectable()
export class LoginService {

    private readonly logger = new Logger(LoginService.name)

    constructor(
        private usersService: UsersService,
        private jwtService: JwtService
    ) { }

    async validateUser(username: string, password: string): Promise<any> {

        const user: IUserFireEntity =
            await this.usersService.findOne('username', username)

        if (!user) {
            return null
        }

        if (user && user.password === password) {
            return user
        }
        return null
    }

    async validateSocialUser(
        user_id: string,
        access_token: string,
        provider: SocialProviders = SocialProviders.FACEBOOK
    ): Promise<any> {

        const user: IUserFireEntity =
            await this.usersService.findOneByDocument(user_id)

        this.logger.debug(this.validateSocialUser.name)
        this.logger.debug(user)

        if (!user) {
            return null
        }

        if (user) {
            // let account =
            //     user.social_accounts.find(f =>
            //         f.provider === provider &&
            //         f.access_token === access_token)

            // this.logger.debug(account)

            // if (account !== undefined) {
            return user
            // }
        }
        return null
    }

    async login(user: any) {

        console.log('auth login')
        console.log(user)

        const payload = { username: user.username, sub: user.userId }

        return {
            username: payload.username,
            access_token: this.jwtService.sign(payload),
        }
    }

    // TODO: Check if social_login already has supplied notification credentials, otherwise add them to the user collections
    // async social_login(body: {
    //     user_id: string,
    //     access_token: string,
    //     device_id: string,
    //     notification_token: string,
    //     platform: DevicePlatforms,
    // }) {
    async social_login(
        user_id: string,
        // access_token: string,
        device_id: string,
        notification_token: string,
        platform: DevicePlatforms,
    ) {
        // const user = await this.usersService.findOneByDocument(user_id)
        const user = await this.usersService.getMongoUserByProp(
            'fire_id',
            user_id
        )

        this.logger.debug(this.social_login.name)
        // console.debug(user)

        let hasCredentials =
            user.notification_credentials
                .some(s =>
                    s.device_id === device_id &&
                    s.notification_token === notification_token &&
                    s.platform === device_id)

        if (!hasCredentials) {
            // TODO: Add credentials
            let res =
                this.usersService
                    .updateMongoUserByValuePair(
                        new ObjectId(user._id),
                        'notification_credentials',
                        [{
                            device_id,
                            notification_token,
                            platform,
                            created: new Date(),
                            updated: new Date(),
                        }])
        }

        const payload = { username: user.username, sub: user.fire_id }

        return {
            _id: user._id,
            username: payload.username,
            email: user.email,
            access_token: this.jwtService.sign(payload),
            mapbox_access_token: process.env.MAPBOX_ACCESS_TOKEN,
            roles: user.roles,
        }
    }

    async social_admin_login(body: { user_id: string, access_token: string }) {
        const user = await this.usersService.findOneByDocument(body.user_id)
        const payload = { username: user.username, sub: user.fire_id }

        return {
            username: payload.username,
            access_token: this.jwtService.sign(payload),
            mapbox_access_token: process.env.MAPBOX_ACCESS_TOKEN,
        }
    }
}