import { Injectable, NotImplementedException, Logger, BadRequestException } from '@nestjs/common'
import { UsersService } from '../../../users/services/users.service'
import { JwtService } from '@nestjs/jwt'
import { ObjectId } from 'mongodb'
import { DevicePlatforms } from 'src/models/common/common'
import { IUserFireEntity } from '../../../users/models'
import { SocialProviders } from '../../../users/models/social-account'
import { checkFbToken } from '../../social/facebook-utility'
import { checkFirebaseUser } from '../../utility/firebase-utility'
import { isObjectEmpty, checkSigningKeyForCredentials } from '../../utility/utility'

@Injectable()
export class AuthService {

    private readonly logger = new Logger(AuthService.name)

    constructor(
        private usersService: UsersService,
        private jwtService: JwtService
    ) { }

    async checkFacebookLogin(
        fbAPIEndpoint: string,
        access_token: string,
        fbAppId: string,
        fbAppSecret: string,
        user_id: string
    ) {
        const data = await checkFbToken(
            fbAPIEndpoint,
            access_token,
            fbAppId,
            fbAppSecret,
        )

        const isEmpty = isObjectEmpty(data)

        if (isEmpty ||
            !data.is_valid ||
            data.app_id !== fbAppId) {
            this.logger.debug(`Either missing properties, or fb data is invalid or app_id doesn't match`)
            console.log(isEmpty)
            console.log(data)
            return false
        }

        const isFirebaseDataOK =
            await checkFirebaseUser(
                'facebook.com',
                user_id,
                data.user_id
            )

        if (!isFirebaseDataOK) {
            return false
        }

        return true
    }
}