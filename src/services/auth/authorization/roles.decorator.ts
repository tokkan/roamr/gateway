import { SetMetadata } from '@nestjs/common'
import { RoleTypes } from 'src/services/users/models'

export const Roles = (...roles: RoleTypes[]) => SetMetadata('roles', roles)
// export const Roles = (...roles: string[]) => SetMetadata('roles', roles)