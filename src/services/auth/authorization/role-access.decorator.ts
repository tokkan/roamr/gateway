import { SetMetadata } from '@nestjs/common'
import { RoleAccessTypes } from 'src/services/users/models'

export const RoleAccess = (...roles: RoleAccessTypes[]) =>
    SetMetadata('role-access', roles)