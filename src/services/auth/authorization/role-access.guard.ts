import { Injectable, CanActivate, ExecutionContext, Logger } from '@nestjs/common'
import { Reflector } from '@nestjs/core'
import { Observable } from 'rxjs'
import { UsersService } from 'src/services/users/services/users.service';
import { inspect } from 'util';

@Injectable()
export class RoleAccessGuard implements CanActivate {

    private readonly logger = new Logger(RoleAccessGuard.name)

    constructor(
        private reflector: Reflector,
        private usersService: UsersService,
    ) { }

    matchRoles(roles: string[], user_roles: string[]) {
        // return user_roles.some(s => roles.some(s2 => s2 === s))
        return roles.every(e => user_roles.includes(e))
    }

    async canActivate(context: ExecutionContext) {
        this.logger.debug(this.canActivate.name)

        let roles =
            this.reflector.get<string[]>('roles', context.getHandler())

        if (roles === undefined ||
            roles.length <= 0) {
            roles =
                this.reflector.get<string[]>('roles', context.getClass())
        }

        const request = context.switchToHttp().getRequest()
        const body = request.body

        const user = await this.usersService.findOneByDocument(body.user_id)

        if (!user) {
            return false
        }

        return this.matchRoles(roles, user.roles)
    }
}