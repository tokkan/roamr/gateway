import { Injectable, CanActivate, ExecutionContext, Logger } from '@nestjs/common'
import { Reflector } from '@nestjs/core'
import { Observable } from 'rxjs'
import { RoleTypes } from 'src/services/users/models'
import { UsersService } from 'src/services/users/services/users.service'

@Injectable()
export class RolesGuard implements CanActivate {

    private readonly logger = new Logger(RolesGuard.name)

    constructor(
        private reflector: Reflector,
        private usersService: UsersService,
    ) { }

    matchRoles(roles: string[], user_roles: RoleTypes[]) {
        // return user_roles.some(s => roles.some(s2 => s2 === s))
        // return roles.every(e => user_roles.includes(e))

        if (user_roles.some(s => s === RoleTypes.ADMIN)) {
            return true
        }

        if (roles.includes(RoleTypes.MOD)) {
            if (user_roles.some(s => s === RoleTypes.MOD)) {
                return true
            } else {
                return false
            }
        }

        if (roles.includes(RoleTypes.AFFILIATE)) {
            if (user_roles.some(s => s === RoleTypes.AFFILIATE)) {
                return true
            } else {
                return false
            }
        }

        return false
    }

    async canActivate(context: ExecutionContext) {
        let roles =
            this.reflector.get<string[]>('roles', context.getHandler())

        if (roles === undefined ||
            roles.length <= 0) {
            roles =
                this.reflector.get<string[]>('roles', context.getClass())
        }

        const request = context.switchToHttp().getRequest()
        let { user_id } = request.body

        if (user_id === undefined) {
            user_id = request.headers.user_id
        }

        const user = await this.usersService.findOneByDocument(user_id)

        if (!user) {
            return false
        }

        return this.matchRoles(roles, user.roles)
    }
}