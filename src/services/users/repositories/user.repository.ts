import { FilterQuery, ObjectId, UpdateOneOptions, UpdateQuery } from 'mongodb'
import { Logger } from '@nestjs/common'
import { objectToValuePairs } from 'src/common/utility/pipe-utility'
import { MongoDatabaseHandle as db, DatabaseModels } from 'src/database/mongodb'
import { IUserMongoEntity, INotificationCredential, IUserEntityBaseArrayMap } from '../models'

const logger = new Logger('UserMongoRepository')

/**
 * 
 * @param user User
 * @param account_id id
 */
export async function createUserMongo(user: IUserMongoEntity) {
    const created = await _createUser(user)

    if (!created || created?.result.ok <= 0) {
        return null
    }
    return created
}

/**
 * Creates a new user
 * @param user User 
 */
async function _createUser(user: IUserMongoEntity) {

    try {
        let c = await db.collection(DatabaseModels.USERS)
        let response = await c.insertOne(user)
        return response

    } catch (error) {
        logger.error(error)
    }
}

export async function checkIfUsernameExists(username: string): Promise<boolean | null> {
    try {
        // const store = await db.connect()

        // let response =
        //     await store
        //         .collection<IUserMongoEntity>(DatabaseModels.USERS)
        //         .find({
        //             username: username
        //         }).count()

        let c = await db.collection(DatabaseModels.USERS)
        let response = await c.find({
            username: username
        }).count()

        if (response && response > 0) {
            return true
        } else {
            return false
        }
    } catch (error) {
        logger.error(error)
        return null
    }
}

export async function getMongoUserByProp<T extends IUserMongoEntity, K extends keyof T>(prop: K, input: T[K]) {
    try {
        // const store = await db.connect()
        // const response =
        //     await store.collection(DatabaseModels.USERS)
        //         .findOne({
        //             [prop]: input
        //         })

        let c = await db.collection(DatabaseModels.USERS)
        const response = await c.findOne({
            [prop]: input
        })

        if (response && response.hasOwnProperty(prop)) {
            return response
        } else {
            return null
        }
    } catch (error) {
        logger.error(error)
        return null
    }
}

export async function getMongoUsersByIds(user_ids: ObjectId[]) {
    try {
        let c = await db.collection(DatabaseModels.USERS)
        let response = await c.find({
            _id: {
                $in: user_ids
            }
        }).toArray()

        if (response && response.length > 0) {
            return response
        } else {
            return []
        }
    } catch (error) {
        logger.error(error)
        return []
    }
}

export async function getUserByUsername(username: string): Promise<IUserMongoEntity> {
    try {
        // const store = await db.connect()

        // let response =
        //     await store
        //         .collection(DatabaseModels.USERS)
        //         .findOne({
        //             username: username
        //         })

        let c = await db.collection(DatabaseModels.USERS)
        let response = await c.findOne({
            username: username
        })

        return response
    } catch (error) {
        logger.error(error)
        return null
    }
}

export async function updateMongoUserByValuePairs<T extends IUserMongoEntity, K extends keyof T>(
    id: ObjectId,
    pairs: {
        prop: K,
        value: T[K]
    }[],
) {
    let props: Partial<IUserMongoEntity> = pairs.reduce((prev, curr) =>
        ({
            ...prev,
            [curr.prop]: curr.value,
        }), {})

    try {
        let c = await db.collection(DatabaseModels.USERS)
        await c.updateOne({
            _id: id
        }, {
            $set: props
        })
    } catch (error) {
        logger.error(error)
        return null
    }
}

// interface IMongoValuePair<T extends INotificationCredential, P extends keyof T> {
//     prop: P
//     value: T[P]
// }

// interface INotificationCredentialValuePairs extends IMongoValuePair<
//     INotificationCredential,
//     keyof INotificationCredential
//     > { }

// export const notificationCredentialsToValuePairs = (
//     obj: INotificationCredential,
// ) => {
//     let entries = Object.entries(obj)
//     // let p: INotificationCredentialValuePairs = {
//     //     prop: 'device_id',
//     //     value: 1,
//     // }
//     let pair = entries.reduce((prev: any, curr: any) => {
//         return [
//             ...prev,
//             {
//                 prop: curr[0],
//                 value: curr[1],
//             }
//         ]
//     }, [])
//     return pair
// }

export async function addToNotificationCredentials(
    user_id: ObjectId,
    entity: INotificationCredential,
    // array_filters: Partial<INotificationCredential>,
) {
    try {
        let c = await db.collection(DatabaseModels.USERS)
        let res =
            await c.updateOne({
                _id: user_id,
            }, {
                $push: {
                    notification_credentials: entity
                }
            })

        logger.debug(addToNotificationCredentials.name)
        console.log(res)
        console.log(entity)

        return res
    } catch (error) {
        return error
    }
}

export async function updateUserElementInArray(
    user_id: ObjectId,
    array_prop: keyof IUserEntityBaseArrayMap,
    entity: INotificationCredential,
    array_filters: Partial<INotificationCredential>,
) {
    let pairs = objectToValuePairs(entity)
    let filters = objectToValuePairs(array_filters)
    let a_filters: object[] =
        filters.length > 0 ?
            filters.reduce((prev, curr) => {
                let value: any
                if (typeof curr.value === typeof ObjectId) {
                    // if (curr.value instanceof ObjectId) {
                    value = new ObjectId(curr.value)
                } else {
                    value = curr.value
                }
                return [
                    ...prev,
                    { [`item.${curr.prop}`]: value },
                ]
            }, [])
            : []

    let query: {
        update: UpdateQuery<IUserMongoEntity> | Partial<UpdateQuery<IUserMongoEntity>>,
        options: UpdateOneOptions,
    } = pairs.reduce((prev, curr) =>
        ({
            update: {
                $addToSet: {
                    // @ts-ignore
                    ...prev.update.$addToSet,
                    [`${array_prop}.$[item].${curr.prop}`]: curr.value
                    // [`${array_prop}.${curr.prop}`]: curr.value
                },
            },
            options: {
                arrayFilters: [{
                    $and: a_filters,
                }],
                upsert: true,
            }
            // options: {
            //     upsert: true,
            // }
        }), {
        update: {},
        options: {},
    })

    logger.debug(updateUserElementInArray.name)
    // logger.debug('params')
    // console.log(pairs)
    // console.log(array_filters)
    logger.debug('query')
    console.log(query)
    // console.log('update')
    // console.log(query.update.$addToSet)
    // console.log('options')
    // // @ts-ignore
    // console.log(query.options.arrayFilters[0].$and)

    try {
        let c = await db.collection(DatabaseModels.USERS)
        let res =
            await c.updateOne({
                _id: user_id,
            },
                query.update,
                query.options
            )

        if (res.upsertedCount <= 0 ||
            res.modifiedCount <= 0) {
            res = await addToNotificationCredentials(
                user_id,
                entity,
            )
        }

        return res
    } catch (error) {
        return error
    }
}

// // TODO: Either have the input be a object and also have a parameter to denote which array property should be used or have it already done before.
// // TODO 1: Add helper function for to change object property to include array prop, like in followship repository 
// // TODO 2: Add array prop directly in query reduce with `${arrayprop}curr[0]` if possible maybe
// // TODO 3: Add arrayfilters as param with keyof INotificationCredentials and so on and create with separate reduce
// export async function updateElementInArray<
//     // K extends keyof Pick<IUserEntityBase, 'notification_credentials' | 'roles' | 'role_access' | 'social_accounts'>,
//     // U extends Omit<IUserEntityBaseArrayMap[K], 'index'>,
//     // P extends keyof Omit<IUserEntityBaseArrayMap[keyof Pick<IUserEntityBase, 'notification_credentials' | 'roles' | 'role_access' | 'social_accounts'>], 'index'>,
//     // // P extends keyof U
//     K extends keyof Pick<IUserEntityBase, 'notification_credentials' | 'roles' | 'role_access' | 'social_accounts'>,
//     U extends IUserEntityBaseArrayMap,
//     P extends keyof IUserEntityBaseArrayMap,
//     >(
//         user_id: ObjectId,
//         pairs: {
//             array_prop: K,
//             // device_id: ObjectId,
//             prop: P,
//             value: U[P]
//         }[],
//         array_filters: {
//             prop: P,
//             value: U[P],
//         }[],
// ) {
//     // let query: {
//     //     filter: FilterQuery<IUserMongoEntity>
//     //     update: UpdateQuery<IUserMongoEntity>
//     //     arrayFilters: object[]
//     //     // } = Object.entries().reduce((prev, curr) =>
//     // } = pairs.reduce((prev, curr) =>
//     //     ({
//     //         filter: {
//     //             user_id: user_id,
//     //         },
//     //         update: {
//     //             $set: {
//     //                 // @ts-ignore
//     //                 ...prev.update.$set,
//     //                 [curr[0]]: curr[1]
//     //             },
//     //         },
//     //         arrayFilters: [{
//     //             $and: [
//     //                 { [`item.target_id`]: new ObjectId(target_id) },
//     //                 { [`item.follower_type`]: target_type },
//     //             ]
//     //         }]
//     //     }), {
//     //     filter: {},
//     //     update: {},
//     //     arrayFilters: [],
//     // })

//     let a_filters: object[] =
//         array_filters.length > 0 ?
//             array_filters.reduce((prev, curr) => {
//                 let value: any
//                 if (typeof curr.value === typeof ObjectId) {
//                     // if (curr.value instanceof ObjectId) {
//                     value = new ObjectId(curr.value)
//                 } else {
//                     value = curr.value
//                 }
//                 return [
//                     ...prev,
//                     { [`item.${curr.prop}`]: value },
//                 ]
//             }, [])
//             : []

//     let query: {
//         update: UpdateQuery<IUserMongoEntity> | Partial<UpdateQuery<IUserMongoEntity>>,
//         options: UpdateOneOptions,
//     } = pairs.reduce((prev, curr) =>
//         ({
//             update: {
//                 $set: {
//                     ...prev,
//                     [`${curr.array_prop}.$[item].${curr.prop}`]: curr.value
//                 },
//             },
//             options: {
//                 arrayFilters: [{
//                     $and: a_filters,
//                 }]
//             }
//         }), {
//         update: {},
//         options: {},
//     })

//     logger.debug(updateElementInArray.name)
//     console.log(query)

//     try {
//         let c = await db.collection(DatabaseModels.USERS)
//         let res =
//             await c.updateOne({
//                 _id: user_id,
//             }, query.update, query.options)
//         return res
//     } catch (error) {
//         return error
//     }
// }