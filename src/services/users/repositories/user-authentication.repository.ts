import { Logger } from '@nestjs/common'
import { MongoDatabaseHandle as db, DatabaseModels } from 'src/database/mongodb'
import { IUserMongoEntity } from '../models'

const logger = new Logger('UserAuthenticationRepository')

// /**
//  * Logs in a user
//  * @param user User
//  */
// export async function loginLocalUser(username: string, password: string)
//     : Promise<ILocalUser | null> {

//     console.log('loginLocalUser')
//     try {
//         // let store = await db.connect()
//         // let response: ILocalUser | null =
//         //     await store
//         //         .collection(DatabaseModels.USERS)
//         //         .findOne({
//         //             username: username,
//         //             password: password
//         //         })

//         let c = await db.collection(DatabaseModels.USERS)
//         let response: ILocalUser | null =
//             await c.findOne({
//                 username: username,
//                 password: password
//             })

//         return response

//     } catch (error) {
//         logger.error(error)
//         return null
//     }
// }

export async function authenticateUser(user: IUserMongoEntity) {
    try {
        // let store = await db.connect()

        // let response: IUserMongoEntity | null | undefined =
        //     await store
        //         ?.collection(DatabaseModels.USERS)
        //         .findOne({
        //             "_id": user._id
        //         })

        let c = await db.collection(DatabaseModels.USERS)
        let response: IUserMongoEntity | null | undefined =
            await c.findOne({
                "_id": user._id
            })

        if (response) {
            return true
        } else {
            return false
        }

    } catch (error) {
        logger.error(error)
        return false
    }
}

export async function isUserAuthenticated(user: IUserMongoEntity) {
    try {
        // let store = await db.connect()
        // let response: IUserMongoEntity | null | undefined =
        //     await store
        //         ?.collection(DatabaseModels.USERS)
        //         .findOne({
        //             "_id": user._id
        //         })

        let c = await db.collection(DatabaseModels.USERS)
        let response: IUserMongoEntity | null | undefined =
            await c.findOne({
                "_id": user._id
            })

        if (response) {
            return true
        } else {
            return false
        }
    } catch (error) {
        logger.error(error)
        return false
    }
}