import { IQueryHandler, QueryHandler } from "@nestjs/cqrs"
import { ObjectId } from "mongodb"
import { IUserMongoEntity } from "../models"
import { UsersService } from "../services/users.service"


export class GetUserByIdQuery {
    constructor(
        public readonly user_id: ObjectId,
    ) { }
}

@QueryHandler(GetUserByIdQuery)
export class GetUserByIdQueryHandler implements IQueryHandler<GetUserByIdQuery> {
    constructor(
        private readonly usersService: UsersService,
    ) { }

    async execute(query: GetUserByIdQuery): Promise<IUserMongoEntity> {
        let res =
            await this.usersService
                .findMongoUserById(query.user_id)
        return res
    }
}

export class GetUsersByIdsQuery {
    constructor(
        public readonly user_ids: ObjectId[],
    ) { }
}

@QueryHandler(GetUsersByIdsQuery)
export class GetUsersByIdsQueryHandler implements IQueryHandler<GetUsersByIdsQuery> {
    constructor(
        private readonly usersService: UsersService,
    ) { }

    async execute(query: GetUsersByIdsQuery): Promise<IUserMongoEntity[]> {
        let res =
            await this.usersService
                .findMongoUsersByIds(query.user_ids)
        return res
    }
}