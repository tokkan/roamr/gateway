import { Injectable, Logger } from "@nestjs/common"
import { Saga, ICommand, ofType } from "@nestjs/cqrs"
import { Observable } from "rxjs"
import { map, delay, mergeMap } from 'rxjs/operators'
import { CreateUserEvent } from "../events/users.aggregate"
// import { CreatePlayerCommand } from "../players/players.commands"
import { CreateUserCommand, AddSocialAccountToUserCommand } from "../commands/users.commands"

@Injectable()
export class UserSagas {

    private readonly logger = new Logger(UserSagas.name)

    // @Saga()
    // kappaAttack = (events$: Observable<any>): Observable<ICommand> => {
    //     return events$.pipe(
    //         ofType(AttackEvent),
    //         map((event: any) => new ConsumeStaminaEvent()),
    //     )
    // }

    // @Saga()
    // attack = (events$: Observable<any>): Observable<ICommand> => {
    //     return events$
    //         .pipe(
    //             ofType(AttackEvent),
    //             // delay(1000),
    //             map(event => {
    //                 this.logger.debug('Saga attack func')
    //                 // return new DropAncientItemCommand(event.heroId, itemId);
    //                 // return new ConsumeStaminaEvent()
    //                 // return new AttackEvent
    //                 return new AttackCommand
    //             }),
    //         )
    // }

    @Saga()
    userCreated = (events$: Observable<any>): Observable<ICommand> => {
        return events$
            .pipe(
                ofType(CreateUserEvent),
                // delay(1000),
                map(event => {
                    this.logger.debug('Saga usercreated event func')
                    // this.logger.debug(events$)
                    // this.logger.debug(event)
                    const user_fire_id = event.user_fire_id
                    const user_mongo_id = event.user_mongo_id
                    const username = event.username
                    const password = event.password
                    const email = event.email
                    const account = event.account
                    const first_name = event.first_name
                    const last_name = event.last_name
                    const avatar = event.avatar
                    const avatar_url = event.avatar_url
                    const private_account = event.private_account
                    const use_real_name = event.use_real_name

                    const commands: ICommand[] = [
                        new CreateUserCommand(
                            user_fire_id,
                            user_mongo_id,
                            username,
                            password,
                            email,
                            account,
                            first_name,
                            last_name,
                            avatar,
                            avatar_url,
                            private_account,
                            use_real_name
                        ),
                        // new AddSocialAccountToUserCommand(user_id, access_token),
                    ]

                    return commands
                }),
                mergeMap(f => f),
            )
    }
}