import {
    DevicePlatforms, IFirestoreEntity, IMongoEntity, IMongoStringEntity
} from "./../../../models/common/common"
import { ISocialAccount } from "./social-account"

export enum RoleTypes {
    PLEB = 'pleb',
    AFFILIATE = 'affiliate',
    MOD = 'mod',
    ADMIN = 'admin',
}

export enum RoleAccessTypes {
    READ_PINS = 'read:pins',
    READ_WRITE_PINS = 'read-write:pins',
}

export interface INotificationCredential {
    device_id: string
    notification_token: string
    platform: DevicePlatforms
    created: Date
    updated: Date
}

export interface IUserEntityBase {
    username: string
    password: string
    notification_credentials: INotificationCredential[]
    email: string
    qr_code: string
    social_accounts: ISocialAccount[]
    is_admin: boolean
    is_verified: boolean
    is_account_verified: boolean
    roles: RoleTypes[]
    role_access: RoleAccessTypes[]
    customer_id: string
    first_name: string
    last_name: string
    full_name: string
    avatar: string
    avatar_url: string
    private_account: boolean
    use_real_name: boolean
}

export interface IUserEntityBaseArrayMap {
    notification_credentials: INotificationCredential
    social_accounts: ISocialAccount
    roles: RoleTypes
    role_access: RoleAccessTypes
}

export interface IUserFireEntity extends IUserEntityBase, IFirestoreEntity, IMongoStringEntity {
}

export interface IUserMongoEntity extends IUserEntityBase, IMongoEntity, IFirestoreEntity { }