export const toSnakeCase = (str: string): string =>
    str &&
    str
        .match(/[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g)
        ?.map(x => x.toLowerCase())
        .join('_') || ''

export const toKebabCase = (str: string) =>
    str &&
    str
        .match(/[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g)
        ?.map(x => x.toLowerCase())
        .join('-')

interface IPOJO {
    [key: string]: any
}

export const objectPropertiesToSnakeCase = (obj: IPOJO) => {
    return Object.entries(obj).reduce((prev, curr) => {
        return {
            ...prev,
            [toSnakeCase(curr[0])]: curr[1]
        }
    }, {})
}