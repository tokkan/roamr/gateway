import { createParamDecorator, ExecutionContext, BadRequestException } from '@nestjs/common'
import * as rawBody from "raw-body"

export const PlainBody = createParamDecorator(
    async (_, context: ExecutionContext) => {
        const req = context.switchToHttp().getRequest<import("express").Request>()
        if (!req.readable) {
            throw new BadRequestException("Invalid body")
        }

        const raw = await rawBody.default(req)
        // const raw = await rawBody(req)
        const body = raw.toString("utf8").trim()
        return body
    })