import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
// import { ScheduleModule } from '@nestjs/schedule'
import { TerminusModule } from '@nestjs/terminus'
import { MulterModule } from '@nestjs/platform-express'
import multer from 'multer'
// import { AuthModule, UsersModule, BusinessModule } from './services'
// import { PinController } from './routes/world/pin/pin.controller'
// import { BusinessController } from './routes/business/business.controller'
// import { PostsController } from './routes/social/posts/posts.controller'
import { UsersModule } from './services/users/users.module'
import { AuthModule } from './services/auth/auth.module'
import { AuthController } from './routes/auth/auth.controller'
import { UserController } from './routes/user/user.controller'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { MESSAGE_SERVICES } from 'src'
import { NotificationController } from './routes/notification/notification.controller'
import { NotificationGateway } from './routes/notification/notification.gateway'
import { BullModule } from '@nestjs/bull'
import { BusinessController } from './routes/business/business.controller'
import { SocketGateway } from './routes/socket/socket.gateway'
import { SearchController } from './routes/search/controllers/search.controller'
import { SearchModule } from './routes/search/search.module'
import { MessageModule } from './routes/social/message/message.module'
import { MessageController } from './routes/social/message/message.controller'
import { MessageGateway } from './routes/social/message/message.gateway'
import { PostsController } from './routes/social/post/controllers/post.controller'
import { PostModule } from './routes/social/post/post.module'
import { StorageModule } from './routes/storage/storage.module'
import { StorageController } from './routes/storage/storage.controller'
// import { MunicipalityController } from './routes/world/municipality/municipality.controller'
// import { WorldModule } from './services/world/world.module'
// import { PaymentModule } from './services/payment/payment.module'
// import { PaymentController } from './routes/payment/payment.controller'
// import { ProductsController } from './routes/commerce/products/products.controller'
// import { UserController } from './routes/user/user.controller'
// import { MapboxController } from './routes/world/mapbox/mapbox.controller'
// import { PaymentWebhooksController } from './routes/payment/payment-webhooks.controller'
// import { SubscriptionController } from './routes/commerce/subscription/subscription.controller'
// import { CustomerController } from './routes/commerce/customer/customer.controller'
// import { WorldController } from './routes/world/world.controller'
// import { SocialModule } from './services/social/social.module'
// import { SocialEventsController } from './routes/social/social-events/social-events.controller'
// import { CommonModule } from './services/common/common.module'
// import { SearchModule } from './services/search/search.module'
// import { SearchController } from './routes/search/search.controller'
// import { FollowshipController } from './routes/social/followship/followship.controller'
// import { NotificationController } from './routes/notification/notification.controller'
// import { RemoteNotificationModule } from './services/remote-notification/remote-notification.module'

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    // ScheduleModule.forRoot(),
    TerminusModule,
    MulterModule.register({
      storage: multer.memoryStorage()
    }),
    BullModule.registerQueue({
      name: 'notification_queue',
      redis: {
        host: process.env.REDIS_HOST,
        port: +process.env.REDIS_PORT
      }
    }),
    ClientsModule.register([
      {
        name: MESSAGE_SERVICES.API_SERVICE,
        transport: Transport.TCP,
        options: {
          host: process.env.API_MICROSERVICE_HOST,
          port: +process.env.API_MICROSERVICE_PORT,
        }
      }
    ]),
    AuthModule,
    // BusinessModule,
    // CommonModule,
    MessageModule,
    // PaymentModule,
    PostModule,
    // RemoteNotificationModule,
    SearchModule,
    // SocialModule,
    StorageModule,
    UsersModule,
    // WorldModule,
  ],
  controllers: [
    AuthController,
    BusinessController,
    // CustomerController,
    // FollowshipController,
    // MapboxController,
    MessageController,
    // MunicipalityController,
    // NotificationController,
    NotificationController,
    NotificationGateway,
    // PaymentController,
    // PaymentWebhooksController,
    // PinController,
    PostsController,
    // ProductsController,
    SearchController,
    StorageController,
    // SocialEventsController,
    // SubscriptionController,
    UserController,
    // WorldController,
  ],
  providers: [
    SocketGateway,
    MessageGateway,
    NotificationGateway,
  ],
})
export class AppModule {
  // configure(consumer: MiddlewareConsumer) {
  //   consumer
  //     .apply(FormDataMiddleware)
  //     .forRoutes('business/create-post')
  // }
}