import {
    PipeTransform, Injectable, ArgumentMetadata, BadRequestException, Logger
} from '@nestjs/common'

@Injectable()
export class FormDataPipe implements PipeTransform {
    private readonly logger = new Logger(FormDataPipe.name)
    constructor() { }

    async transform(value: any, metadata: ArgumentMetadata) {
        this.logger.debug('transform')
        console.log(value)
        // let err = null

        // let request = _context.switchToHttp().getRequest()
        let keys = Object.keys(value)
        console.log(keys)

        console.log('before')
        console.log(value.payload)

        keys.forEach(f => {
            value[f] = JSON.parse(value[f])
        })

        console.log('after')
        console.log(value.payload)

        // if (value.hasOwnProperty('payload')) {
        //     const { error } = this.schema.validate(value.payload)
        //     err = error
        // } else {
        //     const { error } = this.schema.validate(value)
        //     err = error
        // }
        // if (err) {
        //     this.logger.debug(err)
        //     throw new BadRequestException('Validation failed')
        // }
        return value
    }
}