import {
    PipeTransform, Injectable, ArgumentMetadata, BadRequestException, Logger
} from '@nestjs/common'
import { ObjectSchema } from 'joi'

@Injectable()
export class JoiValidationPipe implements PipeTransform {
    private readonly logger = new Logger(JoiValidationPipe.name)
    constructor(private schema: ObjectSchema) { }

    async transform(value: any, metadata: ArgumentMetadata) {
        let err = null

        value = JSON.parse(JSON.stringify(value))
        
        if (value.hasOwnProperty('payload')) {
            const { error } = this.schema.validate(value.payload)
            err = error
        } else {
            const { error } = this.schema.validate(value)
            err = error
        }
        if (err) {
            this.logger.debug(err)
            throw new BadRequestException('Validation failed')
        }
        return value
    }
}