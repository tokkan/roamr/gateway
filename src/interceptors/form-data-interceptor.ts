import {
    Injectable, NestInterceptor, ExecutionContext, CallHandler
} from "@nestjs/common"
import { Observable } from "rxjs"
import { map } from "rxjs/operators"

@Injectable()
export class FormDataInterceptor implements NestInterceptor {
    public intercept(_context: ExecutionContext, next: CallHandler): Observable<any> {

        let request = _context.switchToHttp().getRequest()
        let keys = Object.keys(request.body)

        keys.forEach(f => {
            request.body[f] = JSON.parse(request.body[f])
        })

        if (request.body.payload !== undefined &&
            request.body.payload.assets !== undefined) {
            if (typeof request.body.payload.assets === 'string') {
                request.body.payload.assets =
                    JSON.parse(request.body.payload.assets)
            }
        }

        // changing response
        return next.handle().pipe(
            map(value => {
                return value
            }),
        )
    }
}