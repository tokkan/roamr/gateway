import { MongoClient, Db, MongoClientOptions, GridFSBucket } from "mongodb"
import { Logger } from "@nestjs/common"
import { DatabaseModels, IDatabaseModels, ITextIndexMap } from "."

export type Buckets =
    'social_fs' |
    'world_fs'

export interface IDatabase {
    // connectionString: string
}

export class MongoDatabaseHandle implements IDatabase {
    private static readonly logger = new Logger('Mongo Database')
    private static _connectionString: string = ''
    private static _client: MongoClient
    private static _store: Db
    private static _bucketSocial: GridFSBucket
    private static _bucketWorld: GridFSBucket
    private static _options: MongoClientOptions = {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        poolSize: 5
    }

    static async store() {
        if (this._store !== undefined) {
            return this._store
        } else {
            await this.connect()
            return this._store
        }
    }

    /**
     * Connects to a mongodb instance
     * @param dbName Name of the database to connect to
     */
    static async connect(dbName: string = 'roamr-social') {
        try {
            if (this._client !== undefined &&
                this._store !== undefined) {
                // this.logger.debug('Client should be defined')
                // return this._store
            } else {
                this.logger.log('Connecting to MongoDB')
                this._client = await MongoClient.connect(this._connectionString, this._options)
                this.setDatabase(dbName)
                // return this._store
                this.logger.log(`Connected to MongoDB: ${dbName}`)

                this.logger.log('Connecting to MongoDB bucket')
                this._bucketSocial = new GridFSBucket(this._store)
                this.logger.log(`Connected to MongoDB bucket: roamr_social/fs`)
            }

        } catch (error) {
            if (process.env.NODE_ENV !== 'production') {
                this.logger.error(error)
            } else {
                this.logger.error(error)
            }

            // return null
        }
    }

    static bucket(bucketName: Buckets) {
        switch (bucketName) {
            case 'social_fs':
                return this._bucketSocial
            case 'world_fs':
                return this._bucketWorld
        }
    }

    static async collection<
        T extends IDatabaseModels,
        K extends keyof IDatabaseModels
    >(collection: K) {
        try {
            await this.connect()
            return this._store.collection<T[K]>(collection)
        } catch (error) {
            console.log('Failed getting collection')
            console.log(error)
            this.logger.error(error)
            return null
        }
    }

    static async setDatabase(dbName: string = 'roamr') {
        if (this._store === undefined) {
            this._store = this._client.db(dbName)
        }
    }

    /**
     * Closes the database connection 
     */
    static close() {
        if (this._client) {
            this._client.close()
            this.logger.log('MongoDB connection closed')
        }
    }

    /**
     * Sets the connection string for the database
     * @param connectionString database connection string
     */
    static setConnectionString(connectionString: string) {
        this.logger.log('MongoDB connection has been set')
        this._connectionString = connectionString
    }

    /**
     * TEXT_INDEXES: Text indexes get a name of fieldA_text_fieldB_text
     * where fieldA and fieldB are two different fields. 
     * If there are only fieldA, then the name would be fieldA_text
     * https://docs.mongodb.com/manual/indexes/
     * 
     * ASC_DESC_INDEXES: fieldA_1_fieldB_-1
     * Here fieldA has a ascending index and fieldB a descending index
     */
    static checkIndex() {
        this.logger.debug('Checking MongoDB indexes')

        const textIndexes: ITextIndexMap[] = [
            {
                collection: DatabaseModels.USERS,
                field: [
                    'username',
                    'full_name',
                ]
            }
        ]

        textIndexes.forEach(async f => {
            try {
                let c = await this.collection(f.collection)
                let index_name =
                    f.field.reduce((acc, curr, idx) => {
                        if (idx === 0) {
                            return `${curr}_text`
                        } else {
                            return `${acc}_${curr}_text`
                        }
                    }, '')
                let index_exists = await c.indexExists(index_name)

                if (!index_exists) {
                    let field_spec = f.field.reduce((acc, curr) => {
                        return {
                            ...acc,
                            [curr]: 'text',
                        }
                    }, {})
                    let res = await c.createIndex(field_spec)
                    this.logger.debug(`Created index: ${res}`)
                }
            } catch (error) {
                this.logger.error(error)
            }
        })
    }
}