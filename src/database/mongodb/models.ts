import { IUserMongoEntity } from "src/services/users/models";

export enum DatabaseModels {
    USERS = 'users',
    CUSTOMERS = 'customers',
    SUBSCRIPTIONS = 'subscriptions',
    EVENTS = 'events',
    POSTS = 'posts',
    NOTIFICATIONS = 'notifications',
    NOTIFICATION_SUBSCRIPTIONS = 'notification_subscriptions',
    NOTIFICATION_QUEUE = 'notification_que',
    FOLLOWSHIP = 'follow_relationships',
    REACTIONS = 'reactions',
    COMMENTS = 'comments',
    MAPBOX_REQUESTS = 'mapbox_requests',
    SUPPORT_REQUESTS = 'support_requests',
    SOCIAL_ANALYTICS = 'social_analytics',
    SE_BUSINESS = 'se_business',
    SE_PINS = 'se_pins',
    SE_TRAILS = 'se_trails',
    SE_AREAS = 'se_areas',
    SE_COUNTIES = 'se_counties',
    // SE_COUNTY_CODES = 'se_county_codes',
    // SE_MUNICIPALITY_CODES = 'se_municipality_codes',
    SE_MUNICIPALITIES = 'se_municipalities',
    SE_ZONES = 'se_zones',
}

export enum GenericDatabaseModels {
    USERS = 'users',
    CUSTOMERS = 'customers',
    SUBSCRIPTIONS = 'subscriptions',
    EVENTS = 'events',
    POSTS = 'posts',
    NOTIFICATIONS = 'notifications',
    NOTIFICATION_SUBSCRIPTIONS = 'notification_subscriptions',
    NOTIFICATION_QUEUE = 'notification_que',
    FOLLOWSHIP = 'follow_relationships',
    BUSINESS = 'se_business',
    PINS = 'se_pins',
    TRAILS = 'se_trails',
    MAPBOX_REQUESTS = 'mapbox_requests',
    AREAS = 'se_areas',
    COUNTIES = 'se_counties',
    // COUNTY_CODES = 'se_county_codes',
    // MUNICIPALITY_CODES = 'se_municipality_codes',
    ZONES = 'se_zones',
    MUNICIPALITIES = 'se_municipalities',
}

export interface IDatabaseModels {
    [DatabaseModels.USERS]: IUserMongoEntity
    // [DatabaseModels.CUSTOMERS]: ICustomerEntity
    // [DatabaseModels.SUBSCRIPTIONS]: ISubscriptionMongo
    // [DatabaseModels.EVENTS]: ISocialEventEntity
    // [DatabaseModels.POSTS]: IPostEntity
    // [DatabaseModels.FOLLOWSHIP]: IFollowingEntity
    // [DatabaseModels.REACTIONS]: IReactionEntity
    // [DatabaseModels.COMMENTS]: ICommentThreadEntity
    // [DatabaseModels.MAPBOX_REQUESTS]: IMapboxRequestEntity
    // [DatabaseModels.SUPPORT_REQUESTS]: ISupportTicketEntity
    // [DatabaseModels.SE_BUSINESS]: IBusinessMongoEntity
    // [DatabaseModels.SE_PINS]: IPinEntity
    // [DatabaseModels.SE_TRAILS]: ITrailMongoEntity
    // [DatabaseModels.SE_AREAS]: IAreaMongoEntity
    // [DatabaseModels.SE_MUNICIPALITIES]: IMunicipalityMongoEntity
    // [DatabaseModels.SE_ZONES]: IZoneMongoEntity
}
