import { IDatabaseModels } from "./models"

export interface ITextIndexMap {
    collection: keyof IDatabaseModels
    field: string[]
}

export enum TextIndexes {
    USER_TEXT_INDEX = 'users_text_index',
}

export enum GeoIndexes {
    PIN_GEO_INDEX = 'pins_geo_index',
}