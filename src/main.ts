import { NestFactory } from '@nestjs/core'
import { NestExpressApplication } from '@nestjs/platform-express'
import { Logger } from '@nestjs/common'
import { Request, Response } from 'express'
import { json } from 'body-parser'
import helmet from 'helmet'
import rateLimit from 'express-rate-limit'
import compression from 'compression'
import { AppModule } from './app.module'
import { MongoDatabaseHandle } from './database/mongodb'
import { Firebase } from './database/firebase/database'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
import { Transport } from '@nestjs/microservices'

const logger = new Logger('Gateway')

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule, {
    bodyParser: false,
    // logger: new Logger(),
    // httpsOptions: {
    // }
  })
  const microservice = app.connectMicroservice(
    {
      transport: Transport.TCP,
      port: 3000,
    },
    {
      inheritAppConfig: true
    })
  // const configService = app.get(ConfigService)
  // const PORT = configService.get('PORT')
  const PORT = process.env.PORT || 8080
  // const ORIGINS = process.env.ORIGINS?.split(' ') || []
  const ALLOWED_ORIGINS = process.env.ALLOW_ORIGINS?.split(' ') || []

  app.enableCors({
    origin: ALLOWED_ORIGINS,
    allowedHeaders:
      [
        "Access-Control-Allow-Headers",
        "x-www-form-urlencoded, Origin, X-Requested-With, Content-Type, Accept, Authorization, user_id, _id, target_id, access_token, social_provider, geo_ids"
      ],
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS",
    // preflightContinue: false,
    // credentials: true,
    // optionsSuccessStatus: 204,
  })
  app.use(helmet())
  // app.set('trust proxy', 1)
  app.set('trust proxy', true)
  // app.set('trust proxy', false)
  app.use(
    rateLimit({
      windowMs: 15 * 60 * 1000, // 15 minutes
      max: 100, // limit each IP to 100 requests per windowMs
    })
  )
  app.use(compression({
    // threshold: 1000
  }))
  // app.useWebSocketAdapter(new WsAdapter())

  /**
   * Middleware so paths includes named paths get a raw body, 
   * which e.g. the stripe webhooks need.
   * Other paths are parsed as json, 
   * which is normal behaviour
   */
  app.use((req: Request, res: Response, next: any) => {
    if (req.path.includes('/webhooks/payment')) {
      next()
    } else {
      json()(req, res, next)
    }
  })

  MongoDatabaseHandle.setConnectionString(process.env.DB_CONNECTION_STRING_SOCIAL)
  await MongoDatabaseHandle.connect(process.env.DB_NAME_SOCIAL)
  Firebase.initialize()

  const docOptions = new DocumentBuilder()
    .setTitle('Gateway Docs')
    .setDescription('The Gateway description')
    .setVersion('1.0')
    .addTag('gateway')
    .build()
  const document = SwaggerModule.createDocument(app, docOptions)
  SwaggerModule.setup('api', app, document)

  await app.startAllMicroservicesAsync()
  await app.listen(PORT, async () => {
    setTimeout(async () => {
      logger.log(`Gateway started at port ${PORT}`)
    }, 1000)
  })
}
bootstrap()