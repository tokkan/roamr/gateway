import { InjectQueue } from '@nestjs/bull'
import { Controller, Logger } from '@nestjs/common'
import { EventPattern } from '@nestjs/microservices'
import { ConnectedSocket, MessageBody, SubscribeMessage, WebSocketGateway, WebSocketServer, WsResponse } from '@nestjs/websockets'
import { Queue } from 'bull'
import { ObjectId } from 'mongodb'
import { from, Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { Server, Socket } from 'socket.io'
import { SocketActions } from './actions'

@Controller('notification-gateway')
@WebSocketGateway()
export class NotificationGateway {
  @WebSocketServer()
  server: Server

  private readonly logger = new Logger(NotificationGateway.name)
  // private sockets: ISocket[] = []

  constructor(
    // @InjectQueue('')
    // private readonly notificationQueue: Queue<any>
  ) { }

  // @SubscribeMessage('message')
  // handleMessage(client: Socket, payload: any): string {
  //   return 'Hello world!';
  // }

  // @SubscribeMessage('connection')
  // onConnection(
  //   @MessageBody() data: any,
  //   @ConnectedSocket() client: Socket,
  // ) {
  //   // TODO: Handle connection and connect user to room with '/user/<user_id>. since users can't join from client it should work and since it shouldn't be possible to join from client side.
  //   // TODO: Make the client join different rooms depending on functionality? OR if the client has to listen to a type sent from server to filter out the events sent back? Second probably better since we don't need to have more rooms than necessary

  //   this.logger.debug(this.onConnection.name)
  //   console.log(data)
  //   let user_id = ''
  //   client.join(`user/${user_id}`, () => {
  //     this.logger.debug('sending message to client')
  //     // client.to(`user/${user_id}`).emit('connection', 'Connected to room')
  //     this.server.to(`user/${user_id}`).emit('connected', 'Connected to room')
  //     this.logger.debug('sent message to client')
  //   })
  // }

  onSendFromServer(
    // type: SocketRoomTypes,
    // target_id: string,
    // data: any
    action: SocketActions,
  ) {
    const {
      event,
      data,
    } = action
    let success = this.server
      .to(`${data.type}/${data.user_id}`)
      .emit(event, data)
    return success
  }

  @EventPattern('notification2')
  notifyEvent(data: any) {
    this.logger.debug('n2')
    console.log(data)
    return 2
  }

  @SubscribeMessage('notification')
  onNotification(
    @MessageBody() data: {
      from_id: string
      user_id: string
      notification: {
        type: '',
        payload: any
      }
    }
  ) {
    // this.onSendFromServer({
    //   event: 'notification_count',
    //   data: {
    //     type: 'user',
    //     user_id
    //   }
    // })
  }

  @SubscribeMessage('notification_count')
  onNotificationsCount(
    @MessageBody() data: {
      user_id: string
      count: number
    },
    @ConnectedSocket() client: Socket,
    // ): Observable<WsResponse<number>> {
  ) {
    // TODO: See if microservice can push data here, and send to client? so that the client only has to have the connection open
    // TODO: See possibilities to listen on a queue with RxJS

    this.logger.debug(this.onNotificationsCount.name)
    console.debug(data)

    const event = 'notification_count'
    const response = [1, 3, 2]

    // // client.to('').emit(event)
    // this.server.sockets.to(`user/${data.user_id}`).emit('notification_count', {
    //   count: data.count
    // })
    this.server.to(`user/${data.user_id}`).emit('notification_count', {
      count: data.count
    })

    // return from(response)
    //   .pipe(
    //     map(data => ({ event, data }))
    //   )
  }
}
