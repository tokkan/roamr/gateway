import { ObjectId } from "mongodb"

export type GatewayEventTypes =
    'notification' |
    'notification_count'
// 'instant_message' |
// 'instant_message_count' |
// 'event_new' |
// 'event_nearby' |
// 'event_chat' |
// 'nearby_pin' |
// 'nearby_chat'

export type EventActions =
    INotificationCountEventAction

interface IEventAction<T extends GatewayEventTypes, P> {
    type: T,
    data: P
}

export interface INotificationCountEventAction extends IEventAction<'notification_count', {
    user_id: ObjectId,
    count: number,
}> { }