import { ObjectId } from "mongodb"

export type TargetTypes =
    'user' |
    'event' |
    'pin'

export type GatewaySocketTypes =
    'notification' |
    'notification_count' |
    'instant_message' |
    'instant_message_count' |
    'liked_message' |
    'liked_post' |
    'commented_post' |
    'mentioned_in_post' |
    'looking_for_group' |
    'looking_for_travel_buddy' |
    'looking_for_suggestions' |
    'interest_in_event' |
    'interest_in_location' |
    'popular_event' |
    'popular_location'
// 'event_new' |
// 'event_nearby' |
// 'event_chat' |
// 'nearby_pin' |
// 'nearby_chat'

export type SocketActions =
    INotificationCountSocketAction

interface ISocketPayload {
    [key: string]: any
    type: TargetTypes
    user_id: ObjectId
}

interface ISocketAction<
    E extends GatewaySocketTypes,
    T extends ISocketPayload
    > {
    event: E
    data: T
}

export interface INotificationSocketPayload {
    type: TargetTypes,
    user_id: ObjectId
    notification: {
        title: string
        message: string
    }
}
export interface INotificationSocketAction extends ISocketAction<
    'notification',
    INotificationSocketPayload
    > { }

// export interface INotificationCountSocketAction extends ISocketAction<
//     'notification_count',
//     'user',
//     ObjectId,
//     {
//         // type: 'user',
//         // user_id: ObjectId,
//         count: number,
//     }> { }

export interface INotificationCountSocketAction extends ISocketAction<
    'notification_count',
    {
        type: TargetTypes,
        user_id: ObjectId,
        count: number
    }
    > { }