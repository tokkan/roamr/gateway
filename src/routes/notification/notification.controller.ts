import { Controller, Logger } from '@nestjs/common'
import { EventPattern, MessagePattern } from '@nestjs/microservices'

@Controller('notification')
export class NotificationController {
    private readonly logger = new Logger(NotificationController.name)

    @MessagePattern({
        cmd: 'notification'
    })
    notify(data: any) {
        this.logger.debug('notify controller')
        console.log(data)
        return data
    }

    // @EventPattern('notification2')
    // notifyEvent(data: any) {
    //     this.logger.debug('n2')
    //     console.log(data)
    //     return 2
    // }
}
