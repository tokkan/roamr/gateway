import {
    Controller, Logger, Post, Body, UseGuards, UsePipes, Get, Param, Query, UseInterceptors, Delete, Res, Inject
} from '@nestjs/common'
import { SocialAccountGuard } from 'src/services/auth/social/social-account.guard'
import { RolesGuard } from 'src/services/auth/authorization/roles.guard'
import { Roles } from 'src/services/auth/authorization/roles.decorator'
import { JoiValidationPipe } from 'src/pipes/joi-validation-pipe'
import {
    Routes, createBusinessSchema, ICreateBusinessBody, activateBusinessSchema
} from '.'
import { Response } from 'express'
import { RoleTypes } from 'src/services/users/models'
import { MESSAGE_SERVICES } from 'src'
import { ClientProxy } from '@nestjs/microservices'

// @UseGuards(SocialAccountGuard, RolesGuard)
// @Roles(RoleTypes.AFFILIATE, RoleTypes.MOD, RoleTypes.ADMIN)
@Controller('business')
export class BusinessController {
    private readonly logger = new Logger(BusinessController.name)

    constructor(
        // private readonly businessService: BusinessService,
        @Inject(MESSAGE_SERVICES.API_SERVICE) private client: ClientProxy,
    ) { }

    @Get(Routes.GET_BUSINESS)
    async getBusiness(
        @Param('business_id') business_id: string,
    ) {
        // let response =
        //     await this.businessService.getBusinessById(business_id)
        // return response

        console.log('plz')
        this.logger.debug('hallow')
        return this.client.send(
            { cmd: 'sum' },
            // { data: [1, 2, 3] }
            [1, 2, 3]
        )
    }

    @Get(Routes.GET_BUSINESSES_FOR_USER)
    async getBusinessesForUser(
        @Param('user_id') user_id: string,
    ) {
        // let response =
        //     await this.businessService.getBusinessForUser(user_id)
        // return response
    }

    @Post(Routes.APPLY_AFFILIATE)
    async applyForAffiliate() {
    }

    // @UsePipes(new JoiValidationPipe(businessRequestQuerySchema))
    @Get(Routes.GET_BUSINESS_REQUESTS)
    async getBusinessRequests(
        // @Query() query: IBusinessRequestQuery,
        @Query() query: any,
    ) {
        let { limit, offset } = query

        // let response =
        //     await this.businessService.getBusinessRequests(limit, offset)
        // return response
    }

    @UsePipes(new JoiValidationPipe(createBusinessSchema))
    @Post(Routes.CREATE_BUSINESS)
    async createBusiness(
        @Body() _body: ICreateBusinessBody
    ) {
        console.log(this.createBusiness.name)
        console.log(_body)
        // await this.businessService.createBusinessFromPayload(
        //     _body.user_id,
        //     {
        //         ..._body.payload,
        //     })
    }

    @UsePipes(new JoiValidationPipe(activateBusinessSchema))
    @Post(Routes.ACTIVATE_BUSINESS)
    async activateBusiness(
        @Body() _body: {
            business_id: string
        }
    ) {
        this.logger.debug(this.activateBusiness.name)
        let { business_id } = _body

        if (business_id !== undefined &&
            business_id.length > 0) {
            // return await this.businessService
            //     .activateBusinessEvent(business_id)
        }

        return null
    }

    @Delete('/:id')
    async deleteBusinessById(
        @Res() res: Response,
        @Param('id') id,
    ) {
        this.logger.debug(this.deleteBusinessById.name)
        // let response = await this.businessService.deleteBusinessById(id)
        return res.status(200).send()
    }
}
