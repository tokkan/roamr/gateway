import Joi from "joi"
import { LanguageTypes, CountryTypes } from "src/models/common/common"
import { SocialMediaTypes } from "../social/models/social"
import { PinTypes } from "../world/models/pin"

export enum Routes {
    GET_BUSINESS = '/business/:business_id',
    GET_BUSINESSES_FOR_USER = '/:user_id',
    GET_BUSINESS_REQUESTS = '/business-requests',
    APPLY_AFFILIATE = '/apply-affiliate',
    CREATE_BUSINESS = '/create-business',
    CREATE_POST = '/create-post',
    ACTIVATE_BUSINESS = '/activate',
}

export interface IAuthPayload {
    user_id: string
    access_token: string
    social_provider: string
}

export interface IBusinessRequestQuery {
    limit: number | undefined
    offset: number | undefined
}

export const businessRequestQuerySchema = Joi.object<IBusinessRequestQuery>({
    limit: Joi.number()
        .min(0)
        .max(100)
        .integer()
        .optional(),
    offset: Joi.number()
        .min(0)
        .max(100)
        .integer()
        .optional(),
})

export interface IMunicipalityPayload {
    municipality_fire_id: string
    municipality_mongo_id: string
    municipality_name: string
    municipality_code: string
}

export interface ICreateBusinessPayload {
    zone_id: string
    businessName: string
    sub_title: string
    description: string[]
    language: LanguageTypes
    latitude: number
    longitude: number
    emailAddress: string
    phoneNumber: string
    pin_type: PinTypes
    // sub_pin_types: (PinTypes & SubPinTypes)[]
    sub_pin_types: PinTypes[]
    socialMediaHandle: string
    socialMediaProvider: SocialMediaTypes
    municipality: IMunicipalityPayload
    country: CountryTypes
}

export const createBusinessSchema = Joi.object<ICreateBusinessPayload>({
    zone_id: Joi.string()
        .allow('')
        .optional(),
    businessName: Joi.string()
        .min(3)
        .max(30)
        .required(),
    sub_title: Joi.string()
        .allow('')
        .optional(),
    description: Joi.string()
        .min(3)
        .max(100)
        .required(),
    language: Joi.string()
        .valid('se', 'en', 'dk', 'nl')
        .max(50)
        .required(),
    latitude: Joi.number()
        .precision(20)
        .max(200)
        .min(-200)
        .required(),
    longitude: Joi.number()
        .precision(20)
        .max(200)
        .min(-200)
        .required(),
    emailAddress: Joi.string()
        .email()
        .required(),
    phoneNumber: Joi.string()
        .allow('', '-', '+')
        .alphanum()
        .optional(),
    // pin_type: Joi.string()
    //     .max(30)
    //     // .valid(...Object.values(PinTypes))
    //     .valid(...Object.keys(PinTypes))
    //     .required(),
    pin_type: Joi.string()
        .min(1)
        .max(30)
        .optional(),
    sub_pin_types: Joi.array()
        .min(0)
        .max(20)
        .optional(),
    socialMediaHandle: Joi.string()
        .allow('')
        .alphanum()
        .max(20)
        .optional(),
    socialMediaProvider: Joi.string()
        .max(20),
    municipality: Joi.object<IMunicipalityPayload>({
        municipality_fire_id: Joi.string()
            .min(10)
            .max(100)
            .required(),
        municipality_mongo_id: Joi.string()
            .min(24)
            .max(24)
            .required(),
        municipality_name: Joi.string()
            .min(2)
            .max(30)
            .required(),
        municipality_code: Joi.string()
            .length(4)
            .required()
    }),
    country: Joi.string()
        .valid('sweden', 'norway', 'iceland',
            'finland', 'germany', 'netherlands',
            'england', 'denmark')
        .min(3)
        .max(40)
        .required(),
})

export interface ICreateBusinessBody extends IAuthPayload {
    payload: ICreateBusinessPayload
}

export const activateBusinessSchema = Joi.object({
    user_id: Joi.string()
        .min(5)
        .max(50)
        .alphanum()
        .required(),
    access_token: Joi.string()
        .min(5)
        .max(500)
        // .alphanum()
        .required(),
    social_provider: Joi.string()
        .min(5)
        .max(50)
        .alphanum()
        .required(),
    business_id: Joi.string()
        .min(5)
        .max(50)
        .alphanum()
        .required()
})