import { ObjectId } from 'mongodb'
import { Buckets, MongoDatabaseHandle as db } from './../../database/mongodb'
import fs from 'fs'
import { Readable } from 'stream'
import { Logger } from '@nestjs/common'

const logger = new Logger('StorageRepository')

export const downloadFileFromGridFS = (
    file_id: ObjectId,
    bucketName: Buckets = 'social_fs',
    options?: {
        start: number
        end: number
    }
) => {
    try {
        const bucket = db.bucket(bucketName)
        const stream = bucket.openDownloadStream(
            file_id,
            options,
        )
        return stream
    } catch (error) {
        console.debug(error)
        logger.error(error)
        return null
    }
}

export const uploadFileToGridFS = (
    filename: string,
    buffer: Buffer,
    metadata?: {
        [key: string]: string,
        contentType?: string
    },
    bucketName: Buckets = 'social_fs',
) => {
    try {
        const _id = new ObjectId()
        const bucket = db.bucket(bucketName)
        const stream = bucket.openUploadStreamWithId(
            _id,
            filename,
            {
                metadata,
            }
        )
        const readable = new Readable()
        readable.push(buffer)
        readable.push(null)
        readable.pipe(stream)
        return 200
    } catch (error) {
        console.debug(error)
        logger.error(error)
        return null
    }
}