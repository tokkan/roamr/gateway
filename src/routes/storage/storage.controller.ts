import { Controller, Get, Inject, Logger, Query, Res } from '@nestjs/common'
import { ClientProxy } from '@nestjs/microservices'
import { Response } from 'express'
import { GridFSBucketReadStream, ObjectId } from 'mongodb'
import fs from 'fs'
import { MESSAGE_SERVICES } from 'src'
import { SendActions } from '.'
import { downloadFileFromGridFS } from './storage.repository'
import { from, scheduled } from 'rxjs'
import { PassThrough, Writable } from 'stream'

@Controller('storage')
export class StorageController {
    private readonly logger = new Logger(StorageController.name)

    constructor(
        @Inject(MESSAGE_SERVICES.API_SERVICE)
        private readonly client: ClientProxy,
    ) { }

    private send(payload: SendActions) {
        return this.client.send({
            storage: payload.pattern
        }, payload.data)
    }

    // private emit(payload: EmitActions) {
    //     return this.client.emit(payload.pattern, payload.data)
    // }

    // concatStreams(streamArray, streamCounter = streamArray.length) {
    //     return streamArray
    //         .reduce((mergedStream, stream) => {
    //             // pipe each stream of the array into the merged stream
    //             // prevent the automated 'end' event from firing
    //             mergedStream = stream.pipe(mergedStream, { end: false });
    //             // rewrite the 'end' event handler
    //             // Every time one of the stream ends, the counter is decremented.
    //             // Once the counter reaches 0, the mergedstream can emit its 'end' event.
    //             stream.once('end', () => --streamCounter === 0 && mergedStream.emit('end'));
    //             return mergedStream;
    //         }, new PassThrough())
    // }

    @Get()
    getFile(
        @Res() res: Response,
        @Query() query: {
            id: string
        }
    ) {
        const streams: GridFSBucketReadStream[] = []
        res.set('Content-Type', 'application/octet-stream')

        console.debug(this.getFile.name)
        console.debug(query)

        // TODO: Get metadata from collection
        res.setHeader('asset_type', 'image/jpeg')

        const id = new ObjectId(query.id)
        const stream =
            downloadFileFromGridFS(id)

        return stream.pipe(res)
    }
}
