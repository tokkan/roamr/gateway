import { ObjectId } from "mongodb"

export type MessagePatternTypes =
    'get_file_stream'

// export type EventPatternTypes =
//     'create_post'

interface IMessagePattern {
    storage: MessagePatternTypes
}

interface ISendAction<P extends MessagePatternTypes, D> {
    pattern: P
    data: D
}

// interface IEmitAction<P extends EventPatternTypes, D> {
//     pattern: P
//     data: D
// }

export type SendActions =
    ISendGetFileStream

// export type EmitActions =
//     IQueueCreatePost

interface ISendGetFileStream extends ISendAction<
    'get_file_stream',
    {
        file_id: ObjectId,
        options?: {
            start: number
            end: number
        }
    }> { }