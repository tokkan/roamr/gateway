// import Joi from 'joi'
import * as Joi from 'joi'
import { Controller, Logger, Get, Put, Body, UsePipes, Headers, Res, Inject } from '@nestjs/common'
import { Response } from 'express'
import { ObjectId } from 'mongodb'
import { DevicePlatformsEnum, DevicePlatforms } from 'src/models/common/common'
import { JoiValidationPipe } from 'src/pipes/joi-validation-pipe'
import { IUserEntityBase, IUserEntityBaseArrayMap, INotificationCredential } from 'src/services/users/models'
import { UsersService } from 'src/services/users/services/users.service'
import { ClientProxy } from '@nestjs/microservices'
import { MESSAGE_SERVICES } from 'src'

// const updateUserSchema = Joi.object<IUserEntityBase>({
//     // TODO: Implement non-array types here
// })

// const updateUserArraySchema = Joi.object<IUserEntityBaseArrayMap>({
//     notification_credentials: Joi.object<INotificationCredential>({
//         device_id: Joi.string()
//             .min(5)
//             .max(100)
//             .required(),
//         notification_token: Joi.string()
//             .min(5)
//             .max(100)
//             .required(),
//         platform: Joi.string()
//             .valid(...Object.keys(DevicePlatformsEnum))
//             .required(),
//     }),
//     // TODO: Implement other array types
// })

interface INotificationCredentialsSchema {
    _id: string
    device_id: string
    notification_token: string
    platform: DevicePlatforms
}

const notificationCredentialsSchema = Joi.object<INotificationCredentialsSchema>({
    // _id: Joi.string()
    //     .min(24)
    //     .max(24)
    //     .required(),
    // notification_credentials: Joi.object<INotificationCredential>({
    device_id: Joi.string()
        .min(5)
        .max(100)
        .required(),
    notification_token: Joi.string()
        .min(5)
        .max(300)
        .required(),
    platform: Joi.string()
        .valid(...Object.keys(DevicePlatformsEnum))
        .required(),
    // }),
    // TODO: Implement other array types
})

@Controller('user')
export class UserController {
    private readonly logger = new Logger(UserController.name)

    constructor(
        private readonly usersService: UsersService,
        @Inject(MESSAGE_SERVICES.API_SERVICE) private client: ClientProxy,
    ) { }

    @Get('test')
    async test() {
        this.logger.debug('test')
        return this.client.send(
            {
                cmd: 'sum',
            },
            [1, 2, 3]
        )
    }

    @Get()
    async getCustomerId() {
    }

    async updateUser() { }

    async updateUserArray() { }

    @UsePipes(new JoiValidationPipe(notificationCredentialsSchema))
    @Put('notification-credentials')
    async updateUserNotificationCredentials(
        @Res() response: Response,
        @Headers('_id') _id: string,
        @Body() body: {
            device_id: string
            notification_token: string
            platform: DevicePlatforms
        },
    ) {
        this.logger.debug(this.updateUserNotificationCredentials.name)
        console.log(body)
        console.log(_id)

        const m_id = new ObjectId(_id)

        const {
            device_id,
            notification_token,
            platform,
        } = body

        // let pairs: <
        //     T extends INotificationCredential,
        //     K extends keyof T
        //     >({
        //         prop: K,
        //         value: T
        //     }) => [] = [
        //         {
        //             // array_prop: 'notification_credentials',
        //             prop: 'device_id',
        //             value: device_id
        //         }
        //     ]

        // let array_filters = [
        //     {
        //         prop: 'device_id',
        //         value: device_id,
        //     }
        // ]

        // let res =
        //     await this.usersService
        //         .updateNotificationCredentials(m_id, {
        //             prop: 'device_id',
        //             value: device_id
        //         }, array_filters)

        // let res =
        //     await this.usersService
        //         .updateNotificationCredentials(m_id, [{
        //             prop: 'device_id',
        //             value: device_id
        //         }, {
        //             prop: ''
        //         }], [{
        //             prop: 'device_id',
        //             value: device_id,
        //         }])

        let res =
            await this.usersService
                .updateNotificationCredentials(m_id, {
                    device_id,
                    notification_token,
                    platform,
                    created: new Date(),
                    updated: new Date(),
                }, {
                    device_id,
                    // notification_token,
                    platform
                })

        response.status(200).send()

        // let res =
        //     await this.usersService
        //         .updateUserElementInArray(m_id, pairs, array_filters)

        // let res = 
        //     await this.usersService
        //     .updateUserElementInArray(m_id, [
        //         {
        //             array_prop: 'notification_credentials',
        //             prop: 'platform',
        //             value: 'android',
        //         },
        //         {
        //             array_prop: 'notification_credentials',
        //             prop: 'device_id',
        //             value: '',
        //         },
        //     ], [
        //         {
        //             prop: 'device_id',
        //             value: '',
        //         }
        //     ])
        //     // ], [
        //     //     {
        //     //         prop: 'notification_credentials',
        //     //         value: {

        //     //         }
        //     //     }
        //     // ])
    }
}
