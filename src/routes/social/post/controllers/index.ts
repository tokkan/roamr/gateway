import { ObjectId } from "mongodb"
import { PosterTypes, IImageAssetPayload, ITaggedInPost } from ".."

export type MessagePatternTypes =
    'get_posts_for_user_timeline'

export type EventPatternTypes =
    'create_post'

interface ISendAction<P extends MessagePatternTypes, D> {
    pattern: P
    data: D
}

interface IEmitAction<P extends EventPatternTypes, D> {
    pattern: P
    data: D
}

export type SendActions =
    IGetPostsForUserTimeline

export type EmitActions =
    IQueueCreatePost

interface IGetPostsForUserTimeline extends ISendAction<
    'get_posts_for_user_timeline',
    {
        user_id: ObjectId,
        limit: number
        skip: number
    }> { }

interface IQueueCreatePost extends IEmitAction<
    'create_post',
    {
        payload: {
            poster_id: ObjectId
            poster_user_id: ObjectId
            posted_by_type: PosterTypes
            caption: string
            assets: IImageAssetPayload[]
            is_comments_enabled: boolean
            tagged_in_post: ITaggedInPost[]
        },
        type: 'posts',
        assets: object[]
    }> { }