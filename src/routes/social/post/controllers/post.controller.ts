import { Controller, Logger, Post, UseInterceptors, UsePipes, Body, UseGuards, Get, Res, BadRequestException, Query, Headers, Inject } from '@nestjs/common'
import { FormDataInterceptor } from 'src/interceptors/form-data-interceptor'
import { JoiValidationPipe } from 'src/pipes/joi-validation-pipe'
import { ObjectId } from 'mongodb'
import { PostService } from './../services/post.service'
import { FilesInterceptor } from '@nestjs/platform-express'
import { SocialAccountGuard } from 'src/services/auth/social/social-account.guard'
import { Response } from 'express'
import { createPostSchema, ICreatePostBody, IImageAssetPayload, ITaggedInPost, PosterTypes } from '..'
import { ClientProxy } from '@nestjs/microservices'
import { MESSAGE_SERVICES } from 'src'
import { EmitActions, SendActions } from '.'
import fs from 'fs'

// @UseGuards(SocialAccountGuard)
@Controller('posts')
export class PostsController {
    private readonly logger = new Logger(PostsController.name)

    constructor(
        private readonly postsService: PostService,
        @Inject(MESSAGE_SERVICES.API_SERVICE)
        private readonly client: ClientProxy,
    ) { }

    private send(payload: SendActions) {
        return this.client.send({
            messages: payload.pattern
        }, payload.data)
    }

    private emit(payload: EmitActions) {
        return this.client.emit(payload.pattern, payload.data)
    }

    // @Get('/:id')
    // async getPostById() {
    //     // TODO: First try to just get posts
    //     // TODO: Get posts of the user
    //     // TODO: Get posts of who the user is following
    //     // TODO: Get sponsored posts and content
    //     // TODO: Get some (on the fewer side) sponsored events
    //     // TODO: Get ads?
    // }

    @Get('image')
    async getImage(
        @Res() res: Response,
    ) {
        let response =
            await this.send({
                pattern: 'get_posts_for_user_timeline',
                data: {
                    user_id: new ObjectId('5fbacea7c09c3e7103d886b5'),
                    limit: 1,
                    skip: 0
                }
            }).toPromise()

        let image = response[0].images[0]
        console.debug('image')

        const buffer = Buffer.from(image)

        res.setHeader(
            'Content-Disposition',
            'attachment; filename=mango.jpeg'
        )
    }

    @Get('/user/:id')
    async getUserPosts() {
    }

    @Get('/timeline')
    async getPostsForUserTimeline(
        @Headers('_id') user_id: string,
        @Query() query: {
            limit: number,
            skip: number,
        }
    ) {
        let id = new ObjectId(user_id)
        let limit = Number(query.limit)
        let skip = Number(query.skip)

        // let res =
        //     await this.postsService
        //         .getPostsForUserTimeline(
        //             id,
        //             _limit,
        //             _skip
        //         )

        this.logger.debug(this.getPostsForUserTimeline.name)
        console.log(user_id)
        console.log(id)

        let res = 
            await this.send({
                pattern: 'get_posts_for_user_timeline',
                data: {
                    user_id: id,
                    limit,
                    skip
                }
            }).toPromise()

        console.log(res)

        return res
        // return {
        //     type: 'posts',
        //     data: res,
        // }
    }

    // https://medium.com/better-programming/nestjs-file-uploading-using-multer-f3021dfed733

    // @UseInterceptors(AnyFilesInterceptor(), FormDataInterceptor)
    @UseInterceptors(FormDataInterceptor)
    @UseInterceptors(FilesInterceptor('assets'))
    @UsePipes(new JoiValidationPipe(createPostSchema))
    @Post()
    async createPost(
        @Res() response: Response,
        @Body() _body: ICreatePostBody,
        // // @UploadedFile() file: any,
        // // @UploadedFiles() files: any
    ) {
        this.logger.debug(this.createPost.name)
        // this.logger.debug(_body.payload)
        let payload: {
            poster_id: ObjectId
            poster_user_id: ObjectId
            posted_by_type: PosterTypes
            caption: string
            assets: IImageAssetPayload[]
            is_comments_enabled: boolean
            tagged_in_post: ITaggedInPost[]
            coordinates: number[]
            is_location_enabled: boolean
        } = {
            ..._body.payload,
            poster_id: new ObjectId(_body.payload.poster_id),
            poster_user_id: new ObjectId(_body.payload.poster_user_id)
        }
        let assets = _body.payload.assets

        // let res = await this.postsService.createPost(payload, 'posts', assets)
        // if (res) {
        //     response.status(200).send()
        // } else {
        //     throw new BadRequestException('Failed to create post')
        // }

        // TODO: Maybe change to create post direct, since we would like a return value if the post succeeded OR
        // TODO: OR have a event listening to the success/error of the post creation and return that to the user?
        // await this.postsService.queueCreatePost(payload, 'posts', assets)
        this.emit({
            pattern: 'create_post',
            data: {
                payload: payload,
                type: 'posts',
                assets: assets,
            }
        })
        response.status(200).send()
    }
}
