import { Inject, Injectable, Logger } from '@nestjs/common'
import { ClientProxy } from '@nestjs/microservices'
import { ObjectId } from 'mongodb'
import { MESSAGE_SERVICES } from 'src'
import { SendActions, EmitActions } from '../controllers'

@Injectable()
export class PostService {
    private readonly logger = new Logger(PostService.name)

    constructor(
        @Inject(MESSAGE_SERVICES.API_SERVICE) private client: ClientProxy,
    ) { }

    send(payload: SendActions) {
        this.client.send(payload.pattern, payload.data)
    }

    emit(payload: EmitActions) {
        this.client.emit(payload.pattern, payload.data)
    }

    async getPostsForUserTimeline(
        user_id: ObjectId,
        limit: number,
        skip: number,
    ) {
        this.send({
            pattern: 'get_posts_for_user_timeline',
            data: {
                user_id,
                limit,
                skip,
            }
        })
    }

    async queueCreatePost(
        payload: any,
        type: 'posts',
        assets: object[],
    ) {
        this.emit({
            pattern: 'create_post',
            data: {
                payload,
                type,
                assets,
            }
        })
    }
}
