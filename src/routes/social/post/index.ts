import { ObjectId } from "mongodb"
import Joi from "joi"

export type PosterTypes =
    'user' |
    'business' |
    'zone' |
    'municipality'

export interface ITaggedInPost {
    _id: ObjectId
    type: 'user' | 'business' | 'zone' | 'municipality'
}


export interface IAuthPayload {
    user_id: string
    access_token: string
    social_provider: string
}

export interface IImageAssetPayload {
    // base64: string
    // width: number
    // height: number
    // size: number
    // mime: string
    // // mime: 'image/jpg' | 'image/png' | 'video/mp4'
    data: string,
    modificationDate: string,
    size: number,
    mime: string,
    height: number,
    width: number,
    path: string,
}

export interface ICreatePostPayload {
    poster_id: string
    poster_user_id: string
    posted_by_type: PosterTypes
    // posted_by_type: "user" | "business" | "zone" | "municipality"
    caption: string
    assets: IImageAssetPayload[]
    is_comments_enabled: boolean
    tagged_in_post: ITaggedInPost[]
    is_location_enabled: boolean
    coordinates: number[]
}

export interface ICreatePostBody extends IAuthPayload {
    payload: ICreatePostPayload
    // assets: any
    assets: {
        data: string,
        modificationDate: string,
        size: number,
        mime: string,
        height: number,
        width: number,
        path: string,
    }[]
}

export const createPostSchema = Joi.object<ICreatePostPayload>({
    poster_id: Joi.string()
        .min(24)
        .max(24)
        .required(),
    poster_user_id: Joi.string()
        .min(24)
        .max(24)
        .required(),
    posted_by_type: Joi.valid('user', 'business', 'zone', 'municipality'),
    caption: Joi.string()
        .min(1)
        .max(100)
        .required(),
    assets: Joi.array()
        .min(1)
        .max(3)
        .required(),
    is_comments_enabled: Joi.boolean()
        .required(),
    tagged_in_post: Joi.array()
        .min(0)
        .max(5)
        .optional(),
    coordinates: Joi.array()
        .min(2)
        .max(2)
        .required(),
    is_location_enabled: Joi.boolean()
        .required()
})