export type SocialMediaTypes =
    'facebook' |
    'twitter' |
    'instagram' |
    'google' |
    'discord' |
    'twitch'