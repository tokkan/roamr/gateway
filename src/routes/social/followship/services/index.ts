import { ObjectId } from "mongodb"
import { FollowerTypes } from ".."

export type FollowshipMessagePatternTypes =
    'get_follow_stats' |
    'get_follow_stats_and_user'

export type FollowshipEventPatternTypes =
    'que_follow_target' |
    'que_unfollow_target' |
    'que_change_followship_options'

interface ISendAction<P extends FollowshipMessagePatternTypes, D> {
    pattern: P
    data: D
}

interface IEmitAction<P extends FollowshipEventPatternTypes, D> {
    pattern: P
    data: D
}

export type SendActions =
    ISendGetFollowStatsAction |
    ISendGetFollowStatsAndUserAction

export type EmitActions =
    IEmitQueFollowTargetAction |
    IEmitQueUnfollowTargetAction |
    IEmitQueChangeFollowshipOptionsAction

interface ISendGetFollowStatsAction extends ISendAction<
    'get_follow_stats', {
        target_id: ObjectId
    }> { }

interface ISendGetFollowStatsAndUserAction extends ISendAction<
    'get_follow_stats_and_user', {
        user_id: ObjectId,
        target_id: ObjectId,
        follower_type: FollowerTypes,
    }> { }

interface IEmitQueFollowTargetAction extends IEmitAction<
    'que_follow_target',
    {
        source_id: ObjectId,
        target_id: ObjectId,
        target_type: FollowerTypes,
    }> { }

interface IEmitQueUnfollowTargetAction extends IEmitAction<
    'que_unfollow_target',
    {
        source_id: ObjectId,
        target_id: ObjectId,
        target_type: FollowerTypes,
    }> { }

interface IEmitQueChangeFollowshipOptionsAction extends IEmitAction<
    'que_change_followship_options',
    {
        target_id: ObjectId,
        source_id: ObjectId,
        target_type: FollowerTypes,
        follower_settings: object,
    }> { }