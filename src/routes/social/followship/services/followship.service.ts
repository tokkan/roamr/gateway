import { Inject, Injectable, Logger } from '@nestjs/common'
import { ClientProxy } from '@nestjs/microservices'
import { ObjectId } from 'mongodb'
import { MESSAGE_SERVICES } from 'src'
import { SendActions, EmitActions } from '.'
import { FollowerTypes } from '..'

@Injectable()
export class FollowshipService {
    private readonly logger = new Logger(FollowshipService.name)

    constructor(
        @Inject(MESSAGE_SERVICES.API_SERVICE) private client: ClientProxy,
    ) { }

    send(payload: SendActions) {
        this.client.send(payload.pattern, payload.data)
    }

    emit(payload: EmitActions) {
        this.client.emit(payload.pattern, payload.data)
    }

    async getFollowStats(
        target_id: ObjectId,
    ) {
        this.send({
            pattern: 'get_follow_stats',
            data: {
                target_id: target_id
            }
        })
    }

    async getFollowStatsAndUserRelated(
        user_id: ObjectId,
        target_id: ObjectId,
        follower_type: FollowerTypes,
    ) {
        this.send({
            pattern: 'get_follow_stats_and_user',
            data: {
                user_id,
                target_id,
                follower_type,
            }
        })
    }

    async queFollowTarget(
        source_id: ObjectId,
        target_id: ObjectId,
        target_type: FollowerTypes,
    ) {
        this.emit({
            pattern: 'que_follow_target',
            data: {
                source_id,
                target_id,
                target_type,
            }
        })
    }

    async queUnfollowTarget(
        source_id: ObjectId,
        target_id: ObjectId,
        target_type: FollowerTypes,
    ) {
        this.emit({
            pattern: 'que_unfollow_target',
            data: {
                source_id,
                target_id,
                target_type,
            }
        })
    }

    async queChangeFollowshipOptions(
        target_id: ObjectId,
        source_id: ObjectId,
        target_type: FollowerTypes,
        follower_settings: object,
    ) {
        this.emit({
            pattern: 'que_change_followship_options',
            data: {
                target_id,
                source_id,
                target_type,
                follower_settings,
            }
        })
    }
}
