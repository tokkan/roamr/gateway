import {
    Controller, Logger, Get, Query, Headers, Post, Body, Res, Put, BadRequestException
} from '@nestjs/common'
import { ObjectId } from 'mongodb'
import { Response } from 'express'
import {
    isStringValidObjectId, mergeObjects, pipe, removeUndefinedFromObject, renameObject
} from 'src/common/utility/pipe-utility'
import {
    FollowerTypes, IFollowNotificationSettingsPayload, IFollowMuteSettingsPayload
} from '..'
import { FollowshipService } from './../services/followship.service'

@Controller('followship')
export class FollowshipController {
    private readonly logger = new Logger(FollowshipController.name)

    constructor(
        private readonly followshipService: FollowshipService,
    ) { }

    @Get()
    async getFollowship(
        @Headers('_id') _id: string,
        @Headers('target_id') target_id: string,
        @Query() query: {
            type: 'followers' | 'following' | 'stats' | 'stats_user',
            follower_type: FollowerTypes,
            limit: number,
            skip: number,
        }
    ) {
        const { type, follower_type } = query
        let tid: ObjectId = undefined
        let user_id: ObjectId = undefined
        if (target_id !== undefined &&
            target_id !== null &&
            target_id.length === 24) {
            tid = new ObjectId(target_id)
        } else {
            // throw new ValidationError('The target id is invalid', null, null)
            throw new BadRequestException(
                'Target id is invalid',
                'Target id is invalid'
            )
        }
        if (_id !== undefined &&
            _id !== null &&
            _id.length === 24) {
            user_id = new ObjectId(_id)
        } else {
            throw new BadRequestException(
                'Target id is invalid',
                'Target id is invalid'
            )
        }

        switch (type) {
            case 'stats':
                return await this.followshipService.getFollowStats(tid)

            case 'stats_user':
                return await this.followshipService
                    .getFollowStatsAndUserRelated(
                        user_id,
                        tid,
                        follower_type
                    )
            case 'followers':
                break
            case 'following':
                break
            default:
                break
        }
    }

    @Post('/follow')
    async followTarget(
        @Res() response: Response,
        @Headers('_id') _id: string,
        @Body() body: {
            // source_user_id: string
            target_id: ObjectId
            target_type: FollowerTypes
        }
    ) {
        const { target_id, target_type } = body
        let source_id = undefined
        source_id = isStringValidObjectId(_id)

        if (source_id === null) {
            throw new BadRequestException(
                'The source id is invalid',
                'The source id is invalid',
            )
        }

        await this.followshipService
            .queFollowTarget(source_id, target_id, target_type)
        return response.status(200).send()
    }

    @Put('/unfollow')
    async unfollowTarget(
        @Res() response: Response,
        @Headers('_id') _id: string,
        @Body() body: {
            target_id: ObjectId
            target_type: FollowerTypes
        }
    ) {
        const { target_id, target_type } = body
        let source_id = undefined
        source_id = isStringValidObjectId(_id)

        if (source_id === null) {
            throw new BadRequestException(
                'The source id is invalid',
                'The source id is invalid',
            )
        }

        await this.followshipService
            .queUnfollowTarget(source_id, target_id, target_type)
        return response.status(200).send()
    }

    @Put('/follow-settings')
    async changeFollowSettings(
        @Res() response: Response,
        @Headers('_id') _id: string,
        @Body() body: {
            target_id: ObjectId
            target_type: FollowerTypes
            notification_settings: IFollowNotificationSettingsPayload
            mute_settings: IFollowMuteSettingsPayload
        }
    ) {
        this.logger.debug(this.changeFollowSettings.name)

        const {
            target_id,
            target_type,
            notification_settings,
            mute_settings,
        } = body
        let source_id = undefined
        source_id = isStringValidObjectId(_id)

        if (source_id === null) {
            throw new BadRequestException(
                'The source id is invalid',
                'The source id is invalid',
            )
        }

        const following_notification_settings =
            pipe(
                removeUndefinedFromObject,
                (arr) => renameObject(arr, 'following.$[item].notification_settings.'),
            )(notification_settings)

        const following_mute_settings =
            pipe(
                removeUndefinedFromObject,
                (arr) => renameObject(arr, 'following.$[item].mute_settings.')
            )(mute_settings)

        const followers_notification_settings =
            pipe(
                removeUndefinedFromObject,
                (arr) => renameObject(arr, 'followers.$[item].notification_settings.'),
            )(notification_settings)

        const followers_mute_settings =
            pipe(
                removeUndefinedFromObject,
                (arr) => renameObject(arr, 'followers.$[item].mute_settings.')
            )(mute_settings)

        // TODO: See if can get a proper type back, maybe by creating a specialized type or something
        let following_settings = mergeObjects(
            following_notification_settings,
            following_mute_settings)

        let follower_settings = mergeObjects(
            followers_notification_settings,
            followers_mute_settings)

        /**
         * Update settings in following by the user following
         */
        await this.followshipService
            .queChangeFollowshipOptions(
                source_id,
                target_id,
                target_type,
                following_settings
            )

        /**
         * Update settings in followers by the user being followed
         * Duplicating data here so some queries becomes more 
         * efficient for e.g. notifications
         */
        await this.followshipService
            .queChangeFollowshipOptions(
                target_id,
                source_id,
                target_type,
                follower_settings
            )
        return response.status(200).send()
    }
}
