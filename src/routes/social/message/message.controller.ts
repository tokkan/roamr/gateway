import { Body, Controller, Get, Headers, Inject, InternalServerErrorException, Logger, Post, Res } from '@nestjs/common'
import { ClientProxy } from '@nestjs/microservices'
import { Response } from 'express'
import { ObjectId } from 'mongodb'
import { MESSAGE_SERVICES } from 'src'
import { EmitActions, SendActions } from '.'
import { IChatUser, IInstantMessage, IInstantMessageChatTypes } from './models'

@Controller('messages')
export class MessageController {
    private readonly logger = new Logger(MessageController.name)

    constructor(
        // private readonly messagesService: MessagesService,
        @Inject(MESSAGE_SERVICES.API_SERVICE)
        private readonly client: ClientProxy,
    ) { }

    private send(payload: SendActions) {
        return this.client.send({
            messages: payload.pattern
        }, payload.data)
    }

    private emit(payload: EmitActions) {
        return this.client.emit(payload.pattern, payload.data)
    }

    @Get('chats')
    async getChatsForUser(
        @Headers('_id') _id: string,
    ) {
        let response =
            await this.send({
                pattern: 'get_chats_for_user',
                data: {
                    _id: _id
                }
            }).toPromise()
        return response
    }

    @Get('chat-messages')
    async getMessagesForChat(
        @Headers('_id') _id: string,
    ) {
        let response =
            await this.send({
                pattern: 'get_chat_messages',
                data: {
                    _id,
                }
            }).toPromise()
        return response
    }

    @Post('create')
    async createInstantMessageChat(
        @Res() res: Response,
        @Body() body: {
            type: IInstantMessageChatTypes,
            participants: IChatUser[],
        },
    ) {
        const {
            type,
            participants,
        } = body

        this.logger.debug(this.createInstantMessageChat.name)
        this.logger.debug(body)

        switch (type) {
            case 'single':
                let statusCode: number = await this.send({
                    pattern: 'create_chat',
                    data: {
                        type,
                        participants,
                    }
                }).toPromise()
                if (statusCode === 200) {
                    res.sendStatus(200)
                } else {
                    throw new InternalServerErrorException('Failed to create chat', 'Failed to create chat')
                }
            case 'group':
            default:
                break
        }
    }
}
