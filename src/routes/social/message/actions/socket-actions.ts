import { ObjectId } from "mongodb"

export type GatewaySocketTypes =
    'instant_message'

export type SocketActions =
    ISendMessageSocketAction

interface IChatMessage {
    message: string
    audio: string
    video: string
}

interface ISocketPayload {
    [key: string]: any
    chat_id: ObjectId
    user_id: ObjectId
    message: string
}

interface ISocketAction<
    E extends GatewaySocketTypes,
    T extends ISocketPayload
    > {
    event: E
    data: T
}

export interface ISendMessageSocketPayload extends ISocketPayload {
}

export interface ISendMessageSocketAction extends ISocketAction<
    'instant_message',
    ISendMessageSocketPayload
    > { }
