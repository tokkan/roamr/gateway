import { InjectQueue } from '@nestjs/bull'
import { Controller, Inject, Logger } from '@nestjs/common'
import { ClientProxy, EventPattern, MessagePattern } from '@nestjs/microservices'
import { Cron, CronExpression } from '@nestjs/schedule'
import { ConnectedSocket, MessageBody, SubscribeMessage, WebSocketGateway, WebSocketServer, WsResponse } from '@nestjs/websockets'
import { Queue } from 'bull'
import { intervalToDuration } from 'date-fns'
import { ObjectId } from 'mongodb'
import { from, Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { Server, Socket } from 'socket.io'
import { MESSAGE_SERVICES } from 'src'
import { EmitActions, SendActions } from '.'
import { SocketActions } from './actions/socket-actions'

interface IChat {
  chat_id: ObjectId
  participants: ObjectId[]
  last_message: Date
}

@Controller('message-gateway')
@WebSocketGateway()
export class MessageGateway {
  @WebSocketServer()
  server: Server

  private readonly logger = new Logger(MessageGateway.name)
  private chats: IChat[]

  constructor(
    // @InjectQueue('')
    // private readonly notificationQueue: Queue<any>
    @Inject(MESSAGE_SERVICES.API_SERVICE)
    private readonly client: ClientProxy,
  ) { }

  private send(payload: SendActions) {
    return this.client.send({
      messages: payload.pattern
    }, payload.data)
  }

  private emit(payload: EmitActions) {
    return this.client.emit(payload.pattern, payload.data)
  }

  onSendFromServer(
    action: SocketActions,
  ) {
    const {
      event,
      data,
    } = action
    let success = this.server
      .to(`${data.type}/${data.user_id}`)
      .emit(event, data)
    return success
  }

  @Cron(CronExpression.EVERY_HOUR)
  private cleanUpChats() {
    const now = new Date()
    this.chats = this.chats.filter(f => {
      const duration = intervalToDuration({
        start: f.last_message,
        end: now,
      })

      if (duration.hours <= 1) {
        return f
      }
    })
  }

  // @MessagePattern('get_chat')
  async getChat(chatId: ObjectId): Promise<IChat> {
    // TODO: Get chat and the participants from there
    let chat =
      this.send({
        pattern: 'get_chat',
        data: {
          chat_id: chatId,
        }
      })
    return await chat.toPromise()
  }

  @EventPattern('send_message')
  saveChatMessage(data: any) {
    this.logger.debug('n2')
    console.log(data)
    return 2
  }

  @SubscribeMessage('send_message')
  async onSendMessage(
    @MessageBody() data: {
      chat_id: string
      from_id: string
      user_id: string
      notification: {
        type: '',
        payload: any
      }
    }
  ) {
    const chat_id = new ObjectId(data.chat_id)
    let chat = await this.getChat(chat_id)

    chat.participants.forEach(f => {
      this.onSendFromServer({
        event: 'instant_message',
        data: {
          chat_id: new ObjectId(),
          user_id: new ObjectId(),
          message: '',
        }
      })
    })    
  }

  // @SubscribeMessage('notification_count')
  onNotificationsCount(
    @MessageBody() data: {
      user_id: string
      count: number
    },
    @ConnectedSocket() client: Socket,
    // ): Observable<WsResponse<number>> {
  ) {
    // TODO: See if microservice can push data here, and send to client? so that the client only has to have the connection open
    // TODO: See possibilities to listen on a queue with RxJS

    this.logger.debug(this.onNotificationsCount.name)
    console.debug(data)

    this.server.to(`user/${data.user_id}`).emit('notification_count', {
      count: data.count
    })
  }
}
