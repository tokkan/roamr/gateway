import { ObjectId } from "mongodb"
import { IChatUser } from "./models"

export type ChatTypes =
    'single' |
    'group'

export type MessagePatternTypes =
    'get_chat' |
    'get_chat_messages' |
    'get_chats_for_user' |
    'create_chat'

export type EventPatternTypes =
    'save_search_analytics'

interface ISendAction<P extends MessagePatternTypes, D> {
    pattern: P
    data: D
}

interface IEmitAction<P extends EventPatternTypes, D> {
    pattern: P
    data: D
}

export type SendActions =
    ISendGetChatsAction |
    ISendGetChatsForUserAction |
    ISendGetChatMessagesAction |
    ISendCreateChatAction

export type EmitActions =
    IEmitSaveSearchAnalytics

interface ISendGetChatsAction extends ISendAction<
    'get_chat', {
        chat_id: ObjectId
    }> { }

interface ISendGetChatsForUserAction extends ISendAction<
    'get_chats_for_user', {
        // _id: ObjectId
        _id: string
    }> { }

interface ISendGetChatMessagesAction extends ISendAction<
    'get_chat_messages', {
        _id: string
    }> { }

interface ISendCreateChatAction extends ISendAction<
    'create_chat', {
        type: ChatTypes
        participants: IChatUser[]
    }> { }

interface IEmitSaveSearchAnalytics extends IEmitAction<
    'save_search_analytics',
    {
        source_id: ObjectId,
        target_id: ObjectId,
    }> { }