import { Module } from "@nestjs/common"
import { ClientsModule, Transport } from "@nestjs/microservices"
import { MESSAGE_SERVICES } from "src"
import { SearchService } from "./services/search.service"

@Module({
    imports: [
        ClientsModule.register([
            {
                name: MESSAGE_SERVICES.API_SERVICE,
                transport: Transport.TCP,
                options: {
                    host: process.env.API_MICROSERVICE_HOST,
                    port: +process.env.API_MICROSERVICE_PORT,
                }
            }
        ]),
    ],
    providers: [
        SearchService,
    ],
    exports: [
        SearchService,
    ]
})
export class SearchModule { }