import { ObjectId } from "mongodb"

export type SearchTypes =
    'all' |
    'users' |
    'location' |
    'business'

export type MessagePatternTypes =
    'search_free_text' |
    'search_users'

export type EventPatternTypes =
    'save_search_analytics'

interface ISendAction<P extends MessagePatternTypes, D> {
    pattern: P
    data: D
}

interface IEmitAction<P extends EventPatternTypes, D> {
    pattern: P
    data: D
}

export type SendActions =
    ISendSearchFreeTextAction |
    ISendSearchUserAction

export type EmitActions =
    IEmitSaveSearchAnalytics

interface ISendSearchFreeTextAction extends ISendAction<
    'search_free_text', {
        text: string
        limit: number
        skip: number
    }> { }

interface ISendSearchUserAction extends ISendAction<
    'search_users', {
        text: string
    }> { }

interface IEmitSaveSearchAnalytics extends IEmitAction<
    'save_search_analytics',
    {
        source_id: ObjectId,
        target_id: ObjectId,
    }> { }