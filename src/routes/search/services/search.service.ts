import { Inject, Injectable, Logger } from '@nestjs/common'
import { ClientProxy } from '@nestjs/microservices'
import { from } from 'rxjs'
import { MESSAGE_SERVICES } from 'src'
import { SendActions, EmitActions } from '.'

@Injectable()
export class SearchService {
    private readonly logger = new Logger(SearchService.name)

    constructor(
        @Inject(MESSAGE_SERVICES.API_SERVICE)
        private readonly client: ClientProxy,
    ) { }

    private send(payload: SendActions) {
        console.debug('send')
        console.debug(payload)
        return this.client.send({
            cmd: payload.pattern
        }, payload.data)
        // return this.client.send(payload.pattern, payload.data)
    }

    private emit(payload: EmitActions) {
        return this.client.emit(payload.pattern, payload.data)
    }

    async searchFreeText(
        text: string,
        limit: number,
        skip: number
    ) {
        let res = this.send({
            pattern: 'search_free_text',
            data: {
                text,
                limit,
                skip,
            }
        })

        console.debug(this.searchFreeText.name)
        console.debug(from(res).subscribe(s => console.debug(s)))

        return res
    }

    async searchFreeTextWithOptions() {
    }

    async searchUsers(
        text: string,
    ) {
        // return this.send({
        //     pattern: 'search_free_text',
        //     data: {
        //         text, 
        //     }
        // })
    }
}
