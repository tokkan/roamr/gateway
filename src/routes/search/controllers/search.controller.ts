import { Controller, Logger, Get, UseGuards, Query, Inject } from '@nestjs/common'
import { ClientProxy } from '@nestjs/microservices'
import { MESSAGE_SERVICES } from 'src'
import { SocialAccountGuard } from 'src/services/auth/social/social-account.guard'
import { EmitActions, SendActions } from '../services'
import { SearchService } from './../services/search.service'

@UseGuards(SocialAccountGuard)
@Controller('search')
export class SearchController {
    private readonly logger = new Logger(SearchController.name)

    constructor(
        private readonly searchService: SearchService,
        @Inject(MESSAGE_SERVICES.API_SERVICE)
        private readonly client: ClientProxy,
    ) { }

    private send(payload: SendActions) {
        return this.client.send({
            cmd: payload.pattern
        }, payload.data)
    }

    private emit(payload: EmitActions) {
        return this.client.emit(payload.pattern, payload.data)
    }

    @Get('/free-text')
    async searchFreeText(
        @Query() query: {
            search: string
            limit: number
            skip: number
        }
    ) {
        this.logger.debug(this.searchFreeText.name)
        let search = query.search
        let limit = Number(query.limit)
        let skip = Number(query.skip)

        // let res = await this.searchService.searchFreeText(search, limit, skip)
        // return res
        return this.send({
            pattern: 'search_free_text',
            data: {
                text: search,
                limit,
                skip,
            }
        })
    }

    @Get('/users')
    async searchUsers(
        @Query() query: {
            search: string
            limit: number
            skip: number
        }
    ) {
        let search = query.search
        let limit = Number(query.limit)
        let skip = Number(query.skip)

        // let res = await this.searchService.searchFreeText(search, limit, skip)
        // return res
        return this.client.send({ cmd: 'search_free_text' }, {
            text: search,
            limit,
            skip,
        })
    }
}
