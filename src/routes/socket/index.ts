// interface ISocket {
//     user_id: ObjectId
//     socket: Socket
// }

// // TODO: Move this somewhere
// enum SocketRooms {
//     notification_count = '',
//     message = '',
// }

export enum SocketRooms {
    USER = 'user/',
    GROUP = 'group/',
}

export type SocketRoomTypes =
    'user' |
    'group'

export type SocketClientActionTypes =
    'connection' |
    'disconnect' |
    'send_message'

export type SocketActionTypes =
    'connection' |
    'disconnect' |
    'notification_count' |
    'notification_reaction' |
    'notification_comment' |
    'notification_' |
    'instant_message_count' |
    'instant_message' | 
    'event_new' |
    'event_nearby' |
    'event_chat' |
    'nearby_pin' |
    'nearby_chat'

export interface ISocketBody {
    auth: any
    type: SocketActionTypes
    data?: any
}

export interface ISocketAction<T extends SocketActionTypes, P> {
    type: T,
    value: P
}

export interface INotificationAction extends ISocketAction<
    'notification_count',
    number
    > { }