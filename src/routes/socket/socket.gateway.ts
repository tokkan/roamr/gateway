import { Logger } from '@nestjs/common'
import {
  ConnectedSocket, MessageBody, OnGatewayConnection, OnGatewayDisconnect, SubscribeMessage, WebSocketGateway, WebSocketServer
} from '@nestjs/websockets'
import { Server, Socket } from 'socket.io'
import { AuthService } from 'src/services/auth/services/auth/auth.service'
import { UsersService } from 'src/services/users/services/users.service'

@WebSocketGateway()
export class SocketGateway implements OnGatewayConnection, OnGatewayDisconnect {
  private readonly logger = new Logger(SocketGateway.name)
  @WebSocketServer() server: Server

  private readonly key: string
  // private readonly fbAPIEndpoint =
  // //     // 'https://graph.facebook.com/v7.0/oauth/access_token?'
  // 'https://graph.facebook.com/oauth/access_token?'
  private readonly fbAPIDebugEndpoint =
    'https://graph.facebook.com/v7.0/debug_token?'
  private readonly fb_app_id: string
  private readonly fb_app_secret: string

  constructor(
    private authService: AuthService,
    private usersService: UsersService,
  ) {
    this.key = process.env.ACCOUNT_KEY
    this.fb_app_id = process.env.FACEBOOK_APP_ID
    this.fb_app_secret = process.env.FACEBOOK_APP_SECRET
  }
  async handleConnection(client: Socket, ...args: any[]) {
    this.logger.debug(this.handleConnection.name)
    console.log(`Client ID: ${client.id}`)
    console.debug(client.handshake.query)

    const {
      user_id,
      access_token,
      social_provider,
    } = client.handshake.query

    if (user_id === undefined ||
      access_token === undefined ||
      social_provider === undefined) {
      this.logger.debug('Invalid params')
      this.disconnectSocket(client)
      this.logger.debug(`Client [${client.id}] is disconnected`)
      return
    }

    if ((social_provider as string).toLowerCase() === 'facebook') {
      const isOK = await this.authService.checkFacebookLogin(
        this.fbAPIDebugEndpoint,
        access_token,
        this.fb_app_id,
        this.fb_app_secret,
        user_id,
      )

      if (!isOK) {
        this.disconnectSocket(client)
        this.logger.debug(`Client [${client.id}] is disconnected`)
        return
      }

      const user =
        await this.usersService.findOneByDocument(user_id)

      if (!user) {
        this.logger.debug(`User wasn't found`)
        this.disconnectSocket(client)
        this.logger.debug(`Client [${client.id}] is disconnected`)
        return
      }
    }
  }

  handleDisconnect(client: Socket) {
    this.logger.debug(this.handleDisconnect.name)
    console.log(client.id)
    this.disconnectSocket(client)
  }

  disconnectSocket(client: Socket) {
    client.leaveAll()
    client.disconnect(true)
  }

  // onSendFromServer(
  //   type: SocketRoomTypes,
  //   target_id: string,
  //   data: any
  // ) {
  //   let success = this.server
  //     .to(`${type}/${target_id}`)
  //     .emit(data)
  //   return success
  // }

  // @UseGuards(SocialAccountWsGuard)
  @SubscribeMessage('connection')
  onConnection(
    @MessageBody() data: {
      user_id: string
    },
    @ConnectedSocket() client: Socket,
  ) {
    // TODO: Handle connection and connect user to room with '/user/<user_id>. since users can't join from client it should work and since it shouldn't be possible to join from client side.
    // TODO: Make the client join different rooms depending on functionality? OR if the client has to listen to a type sent from server to filter out the events sent back? Second probably better since we don't need to have more rooms than necessary

    const {
      user_id
    } = data
    this.logger.debug(this.onConnection.name)
    console.log(client.id)

    client.join(`user/${user_id}`, () => {
      this.logger.debug('sending message to client')
      // client.to(`user/${user_id}`).emit('connection', 'Connected to room')
      this.server.to(`user/${user_id}`).emit('connected', 'Connected to room')
      this.logger.debug('sent message to client')
    })
  }

  // @SubscribeMessage('microservice event to listen for')
  // onNotificationCount(
  //   @MessageBody() body: {
  //     user_id: string,
  //     data: number
  //   },
  // ) {
  //   const {
  //     user_id,
  //     data,
  //   } = body

  //   // this.server.to(`${SocketRooms.USER}${user_id}`)
  //   //   .emit('')
  //   this.onSendFromServer('user', user_id, data)
  // }
}
