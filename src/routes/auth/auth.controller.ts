import { createHmac } from 'crypto'
import {
    Controller, Request, Post, UseGuards, Get, Logger, Body, BadRequestException
} from '@nestjs/common'
// import {
//     AuthService, LocalAuthGuard, UsersService
// } from './../../services'
import { ObjectId } from 'mongodb'
import { Roles } from 'src/services/auth/authorization/roles.decorator'
import { RolesGuard } from 'src/services/auth/authorization/roles.guard'
import { SocialAccountGuard } from 'src/services/auth/social/social-account.guard'
import { AuthService } from 'src/services/auth/services/auth/auth.service'
import { INotificationCredential, RoleTypes, IUserFireEntity } from 'src/services/users/models'
import { UsersService } from 'src/services/users/services/users.service'
import { DevicePlatforms } from 'src/models/common/common'
import { LoginService } from 'src/services/auth/services/login/login.service'

enum Routes {
    LOGIN = 'auth/login',
    SOCIAL_LOGIN = 'auth/social-login',
    SOCIAL_ROLE_LOGIN = 'auth/social-role-login',
    SOCIAL_ADMIN_LOGIN = 'auth/social-admin-login',
    GET_FIREBASE_CONFIG = 'auth/firebase-config',
    TWITCH_LOGIN = 'auth/twitch-login',
    REGISTER = 'auth/register',
    CHECK_AVAILABILITY = 'auth/check-name',
    REGISTER_FROM_SOCIAL = 'auth/register-from-social',
    GET_PROFILE = 'profile',
}

interface INameCheck<T extends string | boolean> {
    username: T
}

interface ILoginRequest {
    username: string
    password: string
}

enum SocialProviders {
    FACEBOOK = 'facebook',
}

interface ISocialAccountRequest {
    user_id: string
    email: string
    access_token: string
    social_access_token: string
    social_provider: SocialProviders
    // notification_credentials?: INotificationCredential
    device_id: string
    notification_token: string
    platform: DevicePlatforms
    // username: string
    // password: string
}

interface ISocialRegisterRequest {
    user_id: string
    email: string
    access_token: string
    username: string
    first_name: string
    last_name: string
    avatar: string
    avatar_url: string
    private_account: boolean
    use_real_name: boolean
    notification_credentials?: INotificationCredential
}

@Controller()
export class AuthController {
    private readonly logger = new Logger(AuthController.name)
    private readonly key: string

    constructor(
        private readonly loginService: LoginService,
        private readonly usersService: UsersService,
    ) {
        this.key = process.env.ACCOUNT_KEY
    }

    // @UseGuards(LocalAuthGuard)
    // @Post(Routes.LOGIN)
    // async login(@Body() req_body: ILoginRequest) {

    //     this.logger.debug('login')
    //     const body = {
    //         username: req_body.username,
    //         password: req_body.password,
    //     }
    //     return this.authService.login(body)
    // }

    @UseGuards(SocialAccountGuard)
    @Post(Routes.SOCIAL_LOGIN)
    async socialLogin(@Body() req_body: ISocialAccountRequest) {

        this.logger.debug(this.socialLogin.name)

        // const body = {
        //     user_id: req_body.user_id,
        //     access_token: req_body.access_token,
        //     social_access_token: req_body.social_access_token,
        //     social_provider: req_body.social_provider,
        //     notification_credentials: req_body.notification_credentials ?? null,
        // }
        const {
            user_id,
            access_token,
            social_access_token,
            social_provider,
            // notification_credentials,
            device_id,
            notification_token,
            platform,
        } = req_body
        let res = await this.loginService.social_login(
            user_id,
            // access_token,
            // social_access_token,
            // social_provider,
            // notification_credentials.device_id,
            // notification_credentials.notification_token,
            // notification_credentials.platform,
            device_id,
            notification_token,
            platform,
        )

        this.logger.debug('login success?')
        this.logger.debug(res)

        return res
    }

    // @UseGuards(SocialAccountGuard, RolesGuard)
    // @Roles(RoleTypes.ADMIN)
    // @Post(Routes.SOCIAL_ROLE_LOGIN)
    // async socialRoleLogin(@Body() req_body: ISocialAccountRequest) {
    //     // const body = {
    //     //     user_id: req_body.user_id,
    //     //     access_token: req_body.access_token,
    //     //     social_access_token: req_body.social_access_token,
    //     //     social_provider: req_body.social_provider,
    //     //     notification_credentials: req_body.notification_credentials ?? null,
    //     // }
    //     // return await this.authService.social_login(body)
    //     const {
    //         user_id,
    //         access_token,
    //         social_access_token,
    //         social_provider,
    //         notification_credentials,
    //     } = req_body
    //     return await this.authService.social_login(
    //         user_id,
    //         // access_token,
    //         // social_access_token,
    //         // social_provider,
    //         notification_credentials.device_id,
    //         notification_credentials.notification_token,
    //         notification_credentials.platform,
    //     )
    // }

    async _registerFromSocial(body: ISocialRegisterRequest) {
        // const username = new ObjectId().toHexString()
        const password = new ObjectId().toHexString()
        const pw =
            createHmac('sha512', this.key)
                .update(password)
                .digest('hex')

        const mongo_id = new ObjectId()

        const { validation_passed, access_token } =
            await this.usersService.createUserFromFirebase({
                fire_id: body.user_id,
                _id: mongo_id.toHexString(),
                username: body.username,
                password: pw,
                email: body.email,
                access_token: body.access_token,
                first_name: body.first_name,
                last_name: body.last_name,
                avatar: body.avatar,
                avatar_url: body.avatar_url,
                private_account: body.private_account,
                use_real_name: body.use_real_name,
            })

        return {
            validation_passed,
            username: body.username,
            password,
            access_token,
        }
    }

    @Post('auth/check-account')
    async checkAccount(
        @Body() body: ISocialAccountRequest
    ) {
        this.logger.debug(this.checkAccount.name)
        const { user_id, email, access_token } = body
        const res: IUserFireEntity =
            await this.usersService.findOne('fire_id', user_id)

        this.logger.debug('check account succeed:')
        this.logger.debug(res)

        return res !== null ? true : false
    }

    @Post(Routes.REGISTER_FROM_SOCIAL)
    async registerFromSocial(
        @Body() body: ISocialRegisterRequest
    ) {
        this.logger.debug(this.registerFromSocial.name)

        const {
            user_id,
            email,
            access_token,
            username,
            first_name,
            last_name,
            avatar,
            avatar_url,
            private_account,
            use_real_name,
        } = body
        const res: IUserFireEntity =
            await this.usersService.findOne('fire_id', user_id)

        if (res !== null) {
            // const pw =
            //     createHmac('sha512', this.key)
            //         .update(res.password)
            //         .digest('hex')

            return {
                validation_passed: true,
                _id: user_id,
                username: res.username,
                email: res.email,
                access_token
                // password: pw,
            }
        }

        let response = await this._registerFromSocial({
            user_id,
            email,
            access_token,
            username,
            first_name,
            last_name,
            avatar,
            avatar_url,
            private_account,
            use_real_name,
        })

        if (!response.validation_passed) {
            return {
                validation_passed: false,
            }
        } else {
            return {
                validation_passed: true,
                _id: user_id,
                username: response.username,
                access_token: response.access_token,
                // password: response.password,
            }
        }
    }

    @Post(Routes.CHECK_AVAILABILITY)
    async checkNameAvailability(
        @Body() body: INameCheck<string>) {

        const { username } = body

        const res: INameCheck<boolean> =
            await this.usersService.checkIfNamesAreAvailable(username)

        return res
    }
}