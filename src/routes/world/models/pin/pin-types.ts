export type PinTrailTypes =
    'trail' |
    'hiking' |
    'biking'

export type PinFoodTypes =
    'cafe' |
    'restaurant'

export type PinBusinessTypes =
    'business' |
    'camping' |
    'bnb' |
    'store'

export type PinTypes =
    'area' |
    'swimming' |
    'fishing' |
    'rest_area' |
    'heritage' |
    'nature_reserve' |
    'nature_park' |
    'poi' |
    'event' |
    PinBusinessTypes |
    PinFoodTypes |
    PinTrailTypes

export type PinStoreSubTypes =
    'clothing_store' |
    'grocery_store' |
    'product_store'


export type SubPinTypes =
    PinStoreSubTypes
