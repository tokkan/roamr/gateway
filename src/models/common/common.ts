import { ObjectId } from "mongodb"

export interface IFirestoreEntity {
    fire_id: string
}

export interface IMongoEntity {
    _id: ObjectId
}

export interface IMongoStringEntity {
    _id: string
}

export type CountryTypes =
    'sweden' |
    'norway' |
    'finland' |
    'denmark'

export type LanguageTypes =
    'se' |
    'en' |
    'dk' |
    'nl'

export type DevicePlatforms =
    'android' |
    'ios' |
    'windows' |
    'linux' |
    'macos' |
    'web'

export enum DevicePlatformsEnum {
    'android' = 'android',
    'ios' = 'ios',
    'windows' = 'windows',
    'linux' = 'linux',
    'macos' = 'macos',
    'web' = 'web',
}